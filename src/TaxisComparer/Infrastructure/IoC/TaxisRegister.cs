﻿using System.Reflection;
using Autofac;
using TaxisComparer.Infrastructure.Taxis;
using Module = Autofac.Module;

namespace TaxisComparer.Infrastructure.IoC
{
    public class TaxisRegister : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(TaxisRegister).GetTypeInfo().Assembly).Where(t => typeof(ITaxi).IsAssignableFrom(t)).InstancePerLifetimeScope().AsImplementedInterfaces();
        }
    }
}
