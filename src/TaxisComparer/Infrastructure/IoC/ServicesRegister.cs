﻿using Autofac;
using TaxisComparer.Infrastructure.Comparer;
using TaxisComparer.Infrastructure.Geocoding;

namespace TaxisComparer.Infrastructure.IoC
{
    public class ServicesRegister : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PriceComparer>().As<IPriceComparer>();
            builder.RegisterType<Nominatim>().As<IGeocoding>();
        }
    }
}
