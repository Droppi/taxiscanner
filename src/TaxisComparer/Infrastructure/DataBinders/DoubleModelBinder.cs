﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace TaxisComparer.Infrastructure.DataBinders
{
    public class DoubleModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            return context.Metadata.ModelType == typeof(double) ? new DoubleModelBinder() : null;
        }
    }

    public class DoubleModelBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType != typeof(double))
            {
                bindingContext.Result = ModelBindingResult.Failed();
                return Task.FromResult(false);
            }

            var value = bindingContext.ValueProvider.GetValue(bindingContext.FieldName);
            if (value == ValueProviderResult.None)
            {
                bindingContext.Result = ModelBindingResult.Failed();
                return Task.FromResult(false);
            }


            double result;
            if (!double.TryParse(value.Values.FirstOrDefault(), NumberStyles.Number, CultureInfo.InvariantCulture, out result))
            {
                bindingContext.Result = ModelBindingResult.Failed();
                return Task.FromResult(false);
            }

            bindingContext.Result = ModelBindingResult.Success(result);
            return Task.FromResult(result);
        }
    }
}
