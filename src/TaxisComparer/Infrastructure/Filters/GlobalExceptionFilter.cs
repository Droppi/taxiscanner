﻿using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using NLog;
using TaxisComparer.Infrastructure.Common;

namespace TaxisComparer.Infrastructure.Filters
{
    public class GlobalExceptionFilter : IExceptionFilter
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public void OnException(ExceptionContext context)
        {
            var response = new ErrorResponse
            {
                Message = context.Exception.Message,
                StackTrace = context.Exception.StackTrace
            };

            context.Result = new ObjectResult(response)
            {
                StatusCode = (int)HttpStatusCode.InternalServerError,
                DeclaredType = typeof(ErrorResponse)
            };

            Logger.Error(context.Exception, nameof(GlobalExceptionFilter));
        }
    }
}
