﻿namespace TaxisComparer.Infrastructure.Common.Cookie
{
    public static class Cookies
    {
        public const string PhpSessId = "PHPSESSID";
    }
}
