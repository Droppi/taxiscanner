﻿using System;

namespace TaxisComparer.Infrastructure.Common.Cookie
{
    public class Cookie
    {
        public string Name { get; set; }

        public string Value { get; set; }

        public DateTime Expires { get; set; }

        public string Path { get; set; }

        public string Domain { get; set; }

        public static Cookie ParseCookie(string cookieStr)
        {
            if (string.IsNullOrWhiteSpace(cookieStr))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(cookieStr));

            cookieStr = cookieStr.Trim();

            var keyValueSeparatorIndex = cookieStr.IndexOf('=');
            if (keyValueSeparatorIndex < 1)
                return null;

            var cookie = new Cookie
            {
                Name = cookieStr.Substring(0, keyValueSeparatorIndex).Trim()
            };

            FillCookie(cookie, cookieStr.Substring(keyValueSeparatorIndex + 1).Trim());

            return cookie;
        }

        private static void FillCookie(Cookie cookie, string values)
        {
            var sections = values.Split(';');
            if (sections.Length <= 1)
            {
                cookie.Value = values;
                return;
            }

            cookie.Value = sections[0].Trim();
            for (var i = 1; i < sections.Length; i++)
            {
                var cookieValue = sections[i].Split('=');
                if (cookieValue.Length != 2)
                    continue;

                var name = cookieValue[0].Trim();
                var value = cookieValue[1].Trim()
                    ;
                if (name.Equals(nameof(Expires), StringComparison.OrdinalIgnoreCase))
                {
                    DateTime expires;
                    if (DateTime.TryParse(value, out expires))
                    {
                        cookie.Expires = expires;
                    }
                }
                if (name.Equals(nameof(Path), StringComparison.OrdinalIgnoreCase))
                {
                    cookie.Path = value;
                }
                if (name.Equals(nameof(Domain), StringComparison.OrdinalIgnoreCase))
                {
                    cookie.Domain = value;
                }
            }
        }

        public override string ToString()
        {
            return $"{Name}={Value}";
        }
    }
}