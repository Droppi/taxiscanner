﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Rewrite;

namespace TaxisComparer.Infrastructure.Common.RewriteOptions
{
    public class NonWwwRule : IRule
    {
        public void ApplyRule(RewriteContext context)
        {
            var request = context.HttpContext.Request;
            var currentHost = request.Host;
            if (!currentHost.Host.StartsWith("www."))
                return;

            var newHost = new HostString(currentHost.Host.TrimStart("www."), currentHost.Port ?? 80);
            var newUrl = $"{request.Scheme}//{newHost}{request.PathBase}{request.Path}{request.QueryString}";
            context.HttpContext.Response.Redirect(newUrl, true);
            context.Result = RuleResult.ContinueRules;
        }
    }
}