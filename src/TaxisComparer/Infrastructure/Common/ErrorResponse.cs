﻿namespace TaxisComparer.Infrastructure.Common
{
    public class ErrorResponse
    {
        public string Message { get; set; }
        public string StackTrace { get; set; }
    }
}
