﻿namespace TaxisComparer.Infrastructure.Common
{
    public static class MediaType
    {
        public static string XWwwFormUrlencoded = "application/x-www-form-urlencoded";
        public static string Json = "application/json";
        public static string OctetStream = "application/octet-stream";
    }
}
