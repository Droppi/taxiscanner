﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TaxisComparer.Infrastructure.Common
{
    public static class Helper
    {
        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static DateTime FromTimestamp(double timestamp)
        {
            return Epoch.AddSeconds(timestamp);
        }

        public static long ToTimestamp(DateTime value)
        {
            var elapsedTime = value - Epoch;
            return (long) elapsedTime.TotalSeconds;
        }

        public static string TrimStart(this string target, string trimString)
        {
            var result = target;
            while (result.StartsWith(trimString, StringComparison.OrdinalIgnoreCase))
            {
                result = result.Substring(trimString.Length);
            }

            return result;
        }

        public static string TrimEnd(this string target, string trimString)
        {
            var result = target;
            while (result.EndsWith(trimString, StringComparison.OrdinalIgnoreCase))
            {
                result = result.Substring(0, result.Length - trimString.Length);
            }

            return result;
        }

        public static string DictionaryToString<TKey, TValue>(this Dictionary<TKey, TValue> dictionary)
        {
            return string.Join(";", dictionary.Select(x => x.Key + "=" + x.Value).ToArray());
        }

        public static string Escape(this string str)
        {
            const string str2 = "0123456789ABCDEF";
            var length = str.Length;
            var builder = new StringBuilder(length*2);
            var num3 = -1;
            while (++num3 < length)
            {
                var ch = str[num3];
                int num2 = ch;
                if ((((0x41 > num2) || (num2 > 90)) &&
                     ((0x61 > num2) || (num2 > 0x7a))) &&
                    ((0x30 > num2) || (num2 > 0x39)))
                {
                    switch (ch)
                    {
                        case '@':
                        case '*':
                        case '_':
                        case '+':
                        case '-':
                        case '.':
                        case '/':
                            goto Label_0125;
                    }
                    builder.Append('%');
                    if (num2 < 0x100)
                    {
                        builder.Append(str2[num2/0x10]);
                        ch = str2[num2%0x10];
                    }
                    else
                    {
                        builder.Append('u');
                        builder.Append(str2[(num2 >> 12)%0x10]);
                        builder.Append(str2[(num2 >> 8)%0x10]);
                        builder.Append(str2[(num2 >> 4)%0x10]);
                        ch = str2[num2%0x10];
                    }
                }
                Label_0125:
                builder.Append(ch);
            }
            return builder.ToString();
        }
    }
}