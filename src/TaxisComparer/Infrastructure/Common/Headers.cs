﻿namespace TaxisComparer.Infrastructure.Common
{
    public static class Headers
    {
        public const string XRequestedWith = "X-Requested-With";
        public const string XmlHttpRequest = "XMLHttpRequest";
        public const string UserAgent = "User-Agent";
        public const string UserAgentValue = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36";
        public const string Cookie = "Cookie";
        public const string SetCookie = "Set-Cookie";
    }
}
