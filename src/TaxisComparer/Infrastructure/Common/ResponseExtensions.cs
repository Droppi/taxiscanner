﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace TaxisComparer.Infrastructure.Common
{
    public static class ResponseExtensions
    {
        public static List<Cookie.Cookie> GetCookie(this HttpResponseMessage responseMessage)
        {
            var result = new List<Cookie.Cookie>();

            IEnumerable<string> cookieHeader;
            if (responseMessage.Headers.TryGetValues(Headers.SetCookie, out cookieHeader))
            {
                result.AddRange(cookieHeader.Select(Cookie.Cookie.ParseCookie).Where(cookie => cookie != null));
            }

            return result;
        }
    }
}