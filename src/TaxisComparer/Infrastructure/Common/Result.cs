﻿namespace TaxisComparer.Infrastructure.Common
{
    public class Result
    {
        public bool IsSuccess => Code == 0;

        public int Code { get; set; }
        public string Message { get; set; }

        public Result()
        {
        }

        public Result(int code, string message)
        {
            Code = code;
            Message = message;
        }

        public static Result Fail(int code, string message)
        {
            return new Result(code, message);
        }

        public static Result<T> Fail<T>(T value, int code, string message)
        {
            return new Result<T>(value, code, message);
        }

        public static Result Success()
        {
            return new Result(0, string.Empty);
        }

        public static Result<T> Success<T>(T value)
        {
            return new Result<T>(value, 0, string.Empty);
        }
    }

    public class Result<T> : Result
    {
        public T Value { get; private set; }

        protected internal Result(T value, int code, string error)
            : base(code, error)
        {
            Value = value;
        }
    }
}
