﻿using System;
using Newtonsoft.Json;
using NLog;

namespace TaxisComparer.Infrastructure.Common
{
    public static class JsonConverter
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static string SerializeObject(object obj)
        {
            try
            {
                return JsonConvert.SerializeObject(obj);
            }
            catch (Exception exception)
            {
                Logger.Error(exception, $"{nameof(JsonConverter)}|{nameof(SerializeObject)}");
                return null;
            }
        }

        public static object DeserializeObject(string src)
        {
            try
            {
                return JsonConvert.DeserializeObject(src);
            }
            catch (Exception exception)
            {
                Logger.Error(exception, $"{nameof(JsonConverter)}|{nameof(DeserializeObject)}");
                return null;
            }
        }

        public static T DeserializeObject<T>(string src)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(src);
            }
            catch (Exception exception)
            {
                Logger.Error(exception, $"{nameof(JsonConverter)}|{nameof(DeserializeObject)}");
                return default (T);
            }
        }
    }
}
