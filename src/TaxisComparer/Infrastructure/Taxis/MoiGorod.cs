﻿using System;
using System.Globalization;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using NLog;
using TaxisComparer.Infrastructure.Common;
using TaxisComparer.Infrastructure.Geocoding;
using TaxisComparer.Infrastructure.Options;
using TaxisComparer.Models;

namespace TaxisComparer.Infrastructure.Taxis
{
    public class MoiGorod : ITaxi
    {
        public string Name => _options.name;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly MoiGorodOption _options;
        private readonly HttpClient _httpClient;

        public MoiGorod(IOptions<MoiGorodOption> options, HttpClient httpClient)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<TaxiInfo> GetPriceAsync(GeocoderResult fromGeocoderResult, GeocoderResult toGeocoderResult)
        {
            var fromLat = fromGeocoderResult.Lat;
            var fromLon = fromGeocoderResult.Lon;
            var toLat = toGeocoderResult.Lat;
            var toLon = toGeocoderResult.Lon;

            var requestBody =
                $"{nameof(fromLat)}={Math.Round(fromLat, 6).ToString(CultureInfo.InvariantCulture)}&{nameof(fromLon)}={Math.Round(fromLon, 6).ToString(CultureInfo.InvariantCulture)}&{nameof(toLat)}={Math.Round(toLat, 6).ToString(CultureInfo.InvariantCulture)}&{nameof(toLon)}={Math.Round(toLon, 6).ToString(CultureInfo.InvariantCulture)}";
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, _options.priceApi)
                {
                    Content = new StringContent(requestBody, Encoding.UTF8, MediaType.XWwwFormUrlencoded)
                };
                request.Headers.Add(Headers.Cookie,
                    $"{nameof(_options.CITY_USER)}={_options.CITY_USER["Новосибирск"].ToString()}");

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(MoiGorod)}|{nameof(GetPriceAsync)} failed. Request = ""{request}"", RequestBody = ""{
                                requestBody
                            }"", Response = ""{result}"", From = ""{
                                JsonConverter.SerializeObject(fromGeocoderResult)
                            }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                    return null;
                }

                dynamic moiGorodPrice = JsonConverter.DeserializeObject(result);

                if (moiGorodPrice == null || moiGorodPrice.result == null || moiGorodPrice.result.summaryCost == null)
                {
                    Logger.Warn(
                        $@"{nameof(MoiGorod)}|{
                                nameof(GetPriceAsync)
                            }| Price is invalid. Request = ""{request}"", RequestBody = ""{
                                requestBody
                            }"", Response = ""{
                                result
                            }"", From = ""{JsonConverter.SerializeObject(fromGeocoderResult)}"", To = ""{
                                JsonConverter.SerializeObject(toGeocoderResult)
                            }""");
                    return null;
                }

                string estimateStr = moiGorodPrice.result.summaryCost.ToString();
                decimal estimateDec;
                if (!decimal.TryParse(estimateStr, out estimateDec))
                {
                    Logger.Warn(
                        $@"{nameof(MoiGorod)}|{nameof(GetPriceAsync)}| Can't parse price.{nameof(estimateStr)} = ""{
                                estimateStr
                            }"", Request = ""{request}"", RequestBody = ""{requestBody}"", Response = ""{
                                result
                            }"", From = ""{JsonConverter.SerializeObject(fromGeocoderResult)}"", To = ""{
                                JsonConverter.SerializeObject(toGeocoderResult)
                            }""");
                    return null;
                }

                return new TaxiInfo
                {
                    Name = _options.name,
                    Price = estimateDec,
                    Site = _options.site,
                    IsExact = _options.isExact
                };
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(MoiGorod)}|{nameof(GetPriceAsync)}| RequestBody = ""{requestBody}"", From = ""{
                            JsonConverter.SerializeObject(fromGeocoderResult)
                        }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                return null;
            }
        }
    }
}