﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;
using NLog;
using TaxisComparer.Infrastructure.Common;
using TaxisComparer.Infrastructure.Geocoding;
using TaxisComparer.Infrastructure.Options;
using TaxisComparer.Models;

namespace TaxisComparer.Infrastructure.Taxis
{
    public class Globaxi : ITaxi
    {
        public string Name => _options.name;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly GlobaxiOption _options;
        private readonly HttpClient _httpClient;

        public Globaxi(IOptions<GlobaxiOption> options, HttpClient httpClient)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<TaxiInfo> GetPriceAsync(GeocoderResult fromGeocoderResult, GeocoderResult toGeocoderResult)
        {
            var fromStreetId = GetStreetIdAsync(fromGeocoderResult);
            var toStreetId = GetStreetIdAsync(toGeocoderResult);

            var fromAddress = fromStreetId.Result;
            var toAddress = toStreetId.Result;

            if (string.IsNullOrEmpty(fromAddress) || string.IsNullOrEmpty(toAddress))
            {
                Logger.Warn(
                    $@"{nameof(Globaxi)}|{nameof(GetPriceAsync)}| Address is null. From = ""{fromAddress}"", To = ""{
                            toAddress
                        }""");
                return null;
            }

            var requestBody = new GlobaxiBodyRequest
            {
                pickUpAddressId = fromAddress,
                pickUpPlace = $"{fromGeocoderResult.Address.CleanRoad} {fromGeocoderResult.Address.GetHouseNumber}",
                destinationPlaces = new List<DestinationPlaces>
                {
                    new DestinationPlaces
                    {
                        addressId = toAddress,
                        place = $"{toGeocoderResult.Address.CleanRoad} {toGeocoderResult.Address.GetHouseNumber}"
                    }
                }
            };

            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, _options.priceApi)
                {
                    Content = new StringContent(JsonConverter.SerializeObject(new {request = requestBody}),
                        Encoding.UTF8, MediaType.XWwwFormUrlencoded)
                };
                request.Headers.Add(Headers.Cookie, $"{nameof(_options.auth)}={_options.auth}");

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(Globaxi)}|{nameof(GetPriceAsync)} failed. Auth = ""{_options.auth}"", Request = ""{
                                request
                            }"", RequestBody = ""{requestBody}"", Response = ""{result}"", From = ""{
                                JsonConverter.SerializeObject(fromGeocoderResult)
                            }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                    return null;
                }

                dynamic globaxiPrice = JsonConverter.DeserializeObject(result);

                if (globaxiPrice == null || globaxiPrice.bodies == null || globaxiPrice.bodies.Count == 0 ||
                    globaxiPrice.bodies[0] == null || globaxiPrice.bodies[0].priceValue == null)
                {
                    Logger.Warn(
                        $@"{nameof(Globaxi)}|{
                                nameof(GetPriceAsync)
                            }| Price is invalid. Request = ""{request}"", RequestBody = ""{
                                requestBody
                            }"", Response = ""{
                                result
                            }"", From = ""{JsonConverter.SerializeObject(fromGeocoderResult)}"", To = ""{
                                JsonConverter.SerializeObject(toGeocoderResult)
                            }""");
                    return null;
                }

                string estimateStr = globaxiPrice.bodies[0].priceValue.ToString();
                decimal estimateDec;
                if (!decimal.TryParse(estimateStr, out estimateDec))
                {
                    Logger.Warn(
                        $@"{nameof(Globaxi)}|{nameof(GetPriceAsync)}| Can't parse price.{nameof(estimateStr)} = ""{
                                estimateStr
                            }"", Request = ""{request}"", RequestBody = ""{requestBody}"", Response = ""{
                                result
                            }"", From = ""{JsonConverter.SerializeObject(fromGeocoderResult)}"", To = ""{
                                JsonConverter.SerializeObject(toGeocoderResult)
                            }""");
                    return null;
                }

                return new TaxiInfo
                {
                    Name = _options.name,
                    Price = estimateDec,
                    Site = _options.site,
                    IsExact = _options.isExact
                };
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(Globaxi)}|{nameof(GetStreetIdAsync)}| RequestBody = ""{requestBody}"", From = ""{
                            JsonConverter.SerializeObject(fromGeocoderResult)
                        }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                return null;
            }
        }

        private async Task<string> GetStreetIdAsync(GeocoderResult geocoderResult)
        {
            var globaxiGetStreet = new GlobaxiGetStreet
            {
                queryString = $"{geocoderResult.Address.CleanRoad} {geocoderResult.Address.GetHouseNumber}",
                retrieveFavoriteAddresses = false,
                retrieveNearest = false,
                latitude = geocoderResult.Lat,
                longitude = geocoderResult.Lon,
            };

            var requestQuery = QueryHelpers.AddQueryString(_options.addressApi, new Dictionary<string, string>
            {
                {"q", JsonConverter.SerializeObject(new {request = globaxiGetStreet})},
            });

            try
            {
                var request = new HttpRequestMessage(HttpMethod.Get, requestQuery);
                request.Headers.Add(Headers.Cookie, $"{nameof(_options.auth)}={_options.auth}");

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(Globaxi)}|{nameof(GetStreetIdAsync)} failed. Request = ""{request}"", Response = ""{
                                result
                            }"", Address = ""{JsonConverter.SerializeObject(geocoderResult)}""");
                    return null;
                }

                dynamic address = JsonConverter.DeserializeObject(result);

                if (address == null || address.bodies == null || address.bodies.Count == 0 ||
                    address.bodies[0] == null || address.bodies[0][0] == null || address.bodies[0][0].addressId == null)
                {
                    Logger.Warn(
                        $@"{nameof(Globaxi)}|{
                                nameof(GetStreetIdAsync)
                            }| Address is invalid. Request = ""{request}"", Response = ""{result}"", Address = ""{
                                JsonConverter.SerializeObject(geocoderResult)
                            }""");
                    return null;
                }

                return address.bodies[0][0].addressId;
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(Globaxi)}|{nameof(GetStreetIdAsync)}| RequestQuery =""{requestQuery}"", Address = ""{
                            JsonConverter.SerializeObject(geocoderResult)
                        }""");
                return null;
            }
        }

        private struct GlobaxiGetStreet
        {
            public string queryString { get; set; }
            public bool retrieveFavoriteAddresses { get; set; }
            public bool retrieveNearest { get; set; }
            public double latitude { get; set; }
            public double longitude { get; set; }
        }

        private class GlobaxiBodyRequest
        {
            public string id { get; set; }
            public string pickUpTime { get; set; }
            public string pickUpAddressId { get; set; }
            public string pickUpPlace { get; set; }
            public double latitude { get; set; }
            public double longitude { get; set; }
            public string appartment { get; set; }
            public List<DestinationPlaces> destinationPlaces;
            public string description { get; set; }
            public string price { get; set; }
            public double priceValue { get; set; }
            public string priceFormat { get; set; }
            public string distance { get; set; }
            public string vehicle { get; set; }
            public string vehicleClassId { get; set; } = "547d7e848660182d7c8072ec";
            public string vehicleClassName { get; set; }
            public string vehicleTypeId { get; set; } = "547d7e848660182d7c8072ee";
            public string serviceIds { get; set; }
            public string[] services { get; set; }
            public string status { get; set; }
            public string acceptedTime { get; set; }
            public string completedTime { get; set; }
            public double usedBonus { get; set; }
            public double clientAvailableBonus { get; set; }
            public double bonusesClientCanUse { get; set; }
            public string driverPhoneNumber { get; set; }
            public string branchPhoneNumber { get; set; }
            public string contractorId { get; set; }
            public string bindingId { get; set; }
        }

        private struct DestinationPlaces
        {
            public string addressId { get; set; }
            public string place { get; set; }
        }
    }
}