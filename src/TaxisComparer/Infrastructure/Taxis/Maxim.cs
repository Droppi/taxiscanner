﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;
using NLog;
using TaxisComparer.Infrastructure.Common;
using TaxisComparer.Infrastructure.Geocoding;
using TaxisComparer.Infrastructure.Options;
using TaxisComparer.Models;

namespace TaxisComparer.Infrastructure.Taxis
{
    public class Maxim : ITaxi
    {
        public string Name => _options.name;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly MaximOption _options;
        private readonly HttpClient _httpClient;

        public Maxim(IOptions<MaximOption> options, HttpClient httpClient)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<TaxiInfo> GetPriceAsync(GeocoderResult fromGeocoderResult, GeocoderResult toGeocoderResult)
        {
            var fromStreetId = GetStreetIdAsync(fromGeocoderResult);
            var toStreetId = GetStreetIdAsync(toGeocoderResult);

            var startHouse = fromGeocoderResult.Address.GetHouseNumber;
            var endHouse = toGeocoderResult.Address.GetHouseNumber;
            if (string.IsNullOrEmpty(startHouse) || string.IsNullOrEmpty(endHouse))
            {
                Logger.Warn(
                    $@"{nameof(Minitax)}|{
                            nameof(GetPriceAsync)
                        }| Address house is null. From = ""{startHouse}"", To = ""{endHouse}""");
                return null;
            }

            var startStreet = fromStreetId.Result;
            var endStreet = toStreetId.Result;

            if (string.IsNullOrEmpty(startStreet) || string.IsNullOrEmpty(endStreet))
            {
                Logger.Warn(
                    $@"{nameof(Maxim)}|{nameof(GetPriceAsync)}| Address is null. From = ""{startStreet}"", To = ""{
                            endStreet
                        }""");
                return null;
            }

            var requestQuery = QueryHelpers.AddQueryString(_options.api, new Dictionary<string, string>
            {
                {_options.rName, _options.routeApi},
                {_options.targetName, _options.calculateService},
                {nameof(_options.locale), _options.locale},
                {nameof(_options.city), _options.city.ToString()},
                {nameof(startStreet), startStreet},
                {nameof(startHouse), startHouse},
                {nameof(endStreet), endStreet},
                {nameof(endHouse), endHouse}
            });

            try
            {
                var request = new HttpRequestMessage(HttpMethod.Get, requestQuery);

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(Grand)}|{nameof(GetPriceAsync)} failed. Request = ""{request}"", Response = ""{
                                result
                            }"", From = ""{JsonConverter.SerializeObject(fromGeocoderResult)}"", To = ""{
                                JsonConverter.SerializeObject(toGeocoderResult)
                            }""");
                    return null;
                }

                dynamic maximPrice = JsonConverter.DeserializeObject(result);
                if (maximPrice == null || maximPrice.Price == null)
                {
                    Logger.Warn(
                        $@"{nameof(Grand)}|{
                                nameof(GetPriceAsync)
                            }| Price is invalid. Request = ""{request}"", Response = ""{result}"", From = ""{
                                JsonConverter.SerializeObject(fromGeocoderResult)
                            }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                    return null;
                }

                string estimateStr = maximPrice.Price.ToString();
                decimal estimateDec;
                if (!decimal.TryParse(estimateStr, out estimateDec))
                {
                    Logger.Warn(
                        $@"{nameof(Atlas)}|{nameof(GetPriceAsync)}| Can't parse price.{nameof(estimateStr)} = ""{
                                estimateStr
                            }"", Request = ""{request}"", Response = ""{result}"", From = ""{
                                JsonConverter.SerializeObject(fromGeocoderResult)
                            }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                    return null;
                }

                return new TaxiInfo
                {
                    Name = _options.name,
                    Price = estimateDec,
                    Site = _options.site,
                    IsExact = _options.isExact
                };
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(Maxim)}|{nameof(GetStreetIdAsync)}| RequestQuery = ""{requestQuery}"", From = ""{
                            JsonConverter.SerializeObject(fromGeocoderResult)
                        }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                return null;
            }
        }

        private async Task<string> GetStreetIdAsync(GeocoderResult geocoderResult)
        {
            var streetName = geocoderResult.Address.CleanRoad;
            if (string.IsNullOrEmpty(streetName))
                return null;

            var requestQuery = QueryHelpers.AddQueryString(_options.api, new Dictionary<string, string>
            {
                {_options.rName, _options.routeApi},
                {_options.targetName, _options.addressesExService},
                {nameof(_options.locale), _options.locale},
                {nameof(_options.city), _options.city.ToString()},
                {_options.queryName, streetName}
            });

            try
            {
                var request = new HttpRequestMessage(HttpMethod.Get, requestQuery);

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(Maxim)}|{nameof(GetStreetIdAsync)} failed. Request = ""{request}"", Response = ""{
                                result
                            }"", Address = ""{JsonConverter.SerializeObject(geocoderResult)}""");
                    return null;
                }

                dynamic maximAddress = JsonConverter.DeserializeObject(result);

                if (maximAddress == null || maximAddress.Count == null || maximAddress.Count == 0 ||
                    maximAddress[0] == null || (maximAddress[0].ID == null && maximAddress[0].AddressID == null))
                {
                    Logger.Warn(
                        $@"{nameof(Maxim)}|{
                                nameof(GetStreetIdAsync)
                            }| Address is invalid. Request = ""{request}"", Response = ""{result}"", Address = ""{
                                JsonConverter.SerializeObject(geocoderResult)
                            }""");
                    return null;
                }

                return maximAddress[0]?.ID != null ? maximAddress[0]?.ID : maximAddress[0]?.AddressID;
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(Maxim)}|{nameof(GetStreetIdAsync)}| RequestQuery =""{requestQuery}"", Address = ""{
                            JsonConverter.SerializeObject(geocoderResult)
                        }""");
                return null;
            }
        }
    }
}