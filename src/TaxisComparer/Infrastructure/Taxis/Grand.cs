﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;
using NLog;
using TaxisComparer.Infrastructure.Common;
using TaxisComparer.Infrastructure.Geocoding;
using TaxisComparer.Infrastructure.Options;
using TaxisComparer.Models;

namespace TaxisComparer.Infrastructure.Taxis
{
    public class Grand : ITaxi
    {
        public string Name => _options.name;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly GrandOption _options;
        private readonly HttpClient _httpClient;

        public Grand(IOptions<GrandOption> options, HttpClient httpClient)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<TaxiInfo> GetPriceAsync(GeocoderResult fromGeocoderResult, GeocoderResult toGeocoderResult)
        {
            var fromStreetId = GetStreetIdAsync(fromGeocoderResult);
            var toStreetId = GetStreetIdAsync(toGeocoderResult);

            var PickUpAddressId = fromStreetId.Result;
            var DestinationPlaces = toStreetId.Result;

            if (string.IsNullOrEmpty(PickUpAddressId) || string.IsNullOrEmpty(DestinationPlaces))
            {
                Logger.Warn(
                    $@"{nameof(Grand)}|{nameof(GetPriceAsync)}| Address is null. From = ""{PickUpAddressId}"", To = ""{
                            DestinationPlaces
                        }""");
                return null;
            }

            var PickUpLatitude = fromGeocoderResult.Lat;
            var PickUpLongitude = fromGeocoderResult.Lon;

            var requestBody =
                $"{nameof(_options.NearestTime)}={_options.NearestTime}&{nameof(_options.VehicleType)}={_options.VehicleType["Легковой"]}&{nameof(_options.VehicleClass)}={_options.VehicleClass["Стандарт"]}&{nameof(PickUpAddressId)}={PickUpAddressId}&{nameof(PickUpLatitude)}={PickUpLatitude.ToString(CultureInfo.InvariantCulture)}&{nameof(PickUpLongitude)}={PickUpLongitude.ToString(CultureInfo.InvariantCulture)}&{_options.destinationPlacesAddressIdName}={DestinationPlaces}&{_options.destinationPlacesLatitudeName}={toGeocoderResult.Lat.ToString(CultureInfo.InvariantCulture)}&{_options.destinationPlacesLongitudeName}={toGeocoderResult.Lon.ToString(CultureInfo.InvariantCulture)}";
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, _options.priceApi)
                {
                    Content = new StringContent(requestBody, Encoding.UTF8, MediaType.XWwwFormUrlencoded)
                };

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(Grand)}|{nameof(GetPriceAsync)} failed. Request = ""{request}"", RequestBody = ""{
                                requestBody
                            }"", Response = ""{result}"", From = ""{
                                JsonConverter.SerializeObject(fromGeocoderResult)
                            }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                    return null;
                }

                dynamic price = JsonConverter.DeserializeObject(result);

                if (price == null || price.Price == null)
                {
                    Logger.Warn(
                        $@"{nameof(Grand)}|{
                                nameof(GetPriceAsync)
                            }| Price is invalid. Request = ""{request}"", RequestBody = ""{
                                requestBody
                            }"", Response = ""{
                                result
                            }"", From = ""{JsonConverter.SerializeObject(fromGeocoderResult)}"", To = ""{
                                JsonConverter.SerializeObject(toGeocoderResult)
                            }""");
                    return null;
                }

                string estimateOrg = price.Price.ToString();
                var estimateStr = estimateOrg.TrimEnd("р.").Trim();
                decimal estimateDec;
                if (!decimal.TryParse(estimateStr, out estimateDec))
                {
                    Logger.Warn(
                        $@"{nameof(Grand)}|{nameof(GetPriceAsync)}| Can't parse price. {nameof(estimateOrg)} = ""{
                                estimateOrg
                            }"", {nameof(estimateStr)} = ""{estimateStr}"", Request = ""{request}"", RequestBody = ""{
                                requestBody
                            }"", Response = ""{result}"", From = ""{
                                JsonConverter.SerializeObject(fromGeocoderResult)
                            }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                    return null;
                }

                return new TaxiInfo
                {
                    Name = _options.name,
                    Price = estimateDec,
                    Site = _options.site,
                    IsExact = _options.isExact
                };
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(Grand)}|{nameof(GetPriceAsync)}| RequestBody = ""{requestBody}"", From = ""{
                            JsonConverter.SerializeObject(fromGeocoderResult)
                        }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                return null;
            }
        }

        private async Task<string> GetStreetIdAsync(GeocoderResult geocoderResult)
        {
            var query = $"{geocoderResult.Address.CleanRoad}, {geocoderResult.Address.GetHouseNumber}";
            var timestamp = Helper.ToTimestamp(DateTime.Now);

            var requestQuert = QueryHelpers.AddQueryString(_options.addressApi, new Dictionary<string, string>
            {
                {nameof(query), query},
                {nameof(timestamp), timestamp.ToString()}
            });

            try
            {
                var request = new HttpRequestMessage(HttpMethod.Get, requestQuert);

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(Grand)}|{nameof(GetStreetIdAsync)} failed. Request = ""{request}"", Response = ""{
                                result
                            }"", Address = ""{JsonConverter.SerializeObject(geocoderResult)}""");
                    return null;
                }

                dynamic address = JsonConverter.DeserializeObject(result);

                if (address == null || address.items == null || address.items[0] == null || address.items[0].id == null)
                {
                    Logger.Warn(
                        $@"{nameof(Grand)}|{
                                nameof(GetStreetIdAsync)
                            }| Address is invalid. Request = ""{request}"", Response = ""{result}"", Address = ""{
                                JsonConverter.SerializeObject(geocoderResult)
                            }""");
                    return null;
                }

                return address.items[0].id;
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(Grand)}|{nameof(GetStreetIdAsync)}| RequestQuery =""{requestQuert}"", Address = ""{
                            JsonConverter.SerializeObject(geocoderResult)
                        }""");
                return null;
            }
        }

        #region Old

        private async Task<string> GetStreetIdAsyncOld(GeocoderResult geocoderResult)
        {
            var defaultLocality = geocoderResult.Address.City;
            var region = geocoderResult.Address.City;
            var queryString = $"{geocoderResult.Address.CleanRoad} {geocoderResult.Address.GetHouseNumber}";

            var requestQuert = QueryHelpers.AddQueryString(_options.addressApi, new Dictionary<string, string>
            {
                {nameof(defaultLocality), defaultLocality},
                {nameof(region), region},
                {nameof(queryString), queryString},
                {nameof(_options.ignoreBranch), _options.ignoreBranch.ToString()},
            });

            try
            {
                var request = new HttpRequestMessage(HttpMethod.Get, requestQuert);

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(Grand)}|{nameof(GetStreetIdAsync)} failed. Request = ""{request}"", Response = ""{
                                result
                            }"", Address = ""{JsonConverter.SerializeObject(geocoderResult)}""");
                    return null;
                }

                dynamic address = JsonConverter.DeserializeObject(result);

                if (address == null || address.hint == null || address.hint.address == null ||
                    address.hint.address.id == null)
                {
                    Logger.Warn(
                        $@"{nameof(Grand)}|{
                                nameof(GetStreetIdAsync)
                            }| Address is invalid. Request = ""{request}"", Response = ""{result}"", Address = ""{
                                JsonConverter.SerializeObject(geocoderResult)
                            }""");
                    return null;
                }

                return address.hint.address.id;
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(Grand)}|{nameof(GetStreetIdAsync)}| RequestQuery =""{requestQuert}"", Address = ""{
                            JsonConverter.SerializeObject(geocoderResult)
                        }""");
                return null;
            }
        }

        #endregion
    }
}