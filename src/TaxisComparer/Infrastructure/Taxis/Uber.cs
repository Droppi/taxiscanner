﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;
using NLog;
using TaxisComparer.Infrastructure.Common;
using TaxisComparer.Infrastructure.Geocoding;
using TaxisComparer.Infrastructure.Options;
using TaxisComparer.Models;

namespace TaxisComparer.Infrastructure.Taxis
{
    public class Uber : ITaxi
    {
        public string Name => _options.name;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly UberOption _options;
        private readonly HttpClient _httpClient;

        public Uber(IOptions<UberOption> options, HttpClient httpClient)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<TaxiInfo> GetPriceAsync(GeocoderResult fromGeocoderResult, GeocoderResult toGeocoderResult)
        {
            var start_latitude = fromGeocoderResult.Lat;
            var start_longitude = fromGeocoderResult.Lon;
            var end_latitude = toGeocoderResult.Lat;
            var end_longitude = toGeocoderResult.Lon;

            var requestQuery = QueryHelpers.AddQueryString(_options.priceApi, new Dictionary<string, string>
            {
                {nameof(_options.server_token), _options.server_token},
                {nameof(start_latitude), start_latitude.ToString(CultureInfo.InvariantCulture)},
                {nameof(start_longitude), start_longitude.ToString(CultureInfo.InvariantCulture)},
                {nameof(end_latitude), end_latitude.ToString(CultureInfo.InvariantCulture)},
                {nameof(end_longitude), end_longitude.ToString(CultureInfo.InvariantCulture)},
            });

            try
            {
                var request = new HttpRequestMessage(HttpMethod.Get, requestQuery);
                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(Uber)}|{nameof(GetPriceAsync)} failed. Request = ""{request}"", Response = ""{
                                result
                            }"", From = ""{JsonConverter.SerializeObject(fromGeocoderResult)}"", To = ""{
                                JsonConverter.SerializeObject(toGeocoderResult)
                            }""");
                    return null;
                }

                dynamic uberPrice = JsonConverter.DeserializeObject(result);

                if (uberPrice == null || uberPrice.prices == null || uberPrice.prices.Count == 0 ||
                    uberPrice.prices[0] == null || uberPrice.prices[0].high_estimate == null)
                {
                    Logger.Warn(
                        $@"{nameof(Uber)}|{
                                nameof(GetPriceAsync)
                            }| Price is invalid. Request = ""{request}"", Response = ""{result}"", From = ""{
                                JsonConverter.SerializeObject(fromGeocoderResult)
                            }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                    return null;
                }

                string estimateStr = uberPrice.prices[0].high_estimate.ToString();
                decimal estimateDec;
                if (!decimal.TryParse(estimateStr, out estimateDec))
                {
                    Logger.Warn(
                        $@"{nameof(Uber)}|{nameof(GetPriceAsync)}| Can't parse price. {nameof(estimateStr)} = ""{
                                estimateStr
                            }"", Request = ""{request}"", Response = ""{result}"", From = ""{
                                JsonConverter.SerializeObject(fromGeocoderResult)
                            }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                    return null;
                }

                return new TaxiInfo
                {
                    Name = _options.name,
                    Price = estimateDec,
                    Site = _options.site,
                    IsExact = _options.isExact
                };
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(Uber)}|{nameof(GetPriceAsync)}| RequestQuery = ""{requestQuery}"", From = ""{
                            JsonConverter.SerializeObject(fromGeocoderResult)
                        }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                return null;
            }
        }
    }
}