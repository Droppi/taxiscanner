﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using NLog;
using TaxisComparer.Infrastructure.Common;
using TaxisComparer.Infrastructure.Geocoding;
using TaxisComparer.Infrastructure.Options;
using TaxisComparer.Models;

namespace TaxisComparer.Infrastructure.Taxis
{
    public class Saturn : ITaxi
    {
        public string Name => _options.name;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly SaturnOption _options;
        private readonly HttpClient _httpClient;

        public Saturn(IOptions<SaturnOption> options, HttpClient httpClient)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<TaxiInfo> GetPriceAsync(GeocoderResult fromGeocoderResult, GeocoderResult toGeocoderResult)
        {
            var fromAddressResult = GetAddressIdAsync(fromGeocoderResult);
            var toAddressResult = GetAddressIdAsync(toGeocoderResult);

            var fromAddress = fromAddressResult.Result;
            var toAddress = toAddressResult.Result;

            if (fromAddress == null || toAddress == null)
            {
                Logger.Warn(
                    $@"{nameof(Saturn)}|{nameof(GetPriceAsync)}| Address is null. From = ""{
                            JsonConverter.SerializeObject(fromAddress)
                        }"", To = ""{JsonConverter.SerializeObject(toAddress)}""");
                return null;
            }

            var requestBody = new PriceRequest
            {
                orderoptionid = _options.orderoptionid["Легковой обычный Новосибирск"],
                authrec = _options.authrec,
                addressList = new List<Address>
                {
                    fromAddress,
                    toAddress
                }
            };
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, _options.priceApi)
                {
                    Content = new StringContent(JsonConverter.SerializeObject(requestBody), Encoding.UTF8,
                        MediaType.Json)
                };
                request.Headers.Add(Headers.UserAgent, _options.userAgent);

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(Saturn)}|{nameof(GetPriceAsync)} failed. Request = ""{request}"", RequestBody = ""{
                                requestBody
                            }"", Response = ""{result}"", From = ""{
                                JsonConverter.SerializeObject(fromGeocoderResult)
                            }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                    return null;
                }

                dynamic price = JsonConverter.DeserializeObject(result);
                if (price == null || price.estimCostList == null || price.estimCostList.Count == 0
                    || price.estimCostList[0].cost == null)
                {
                    Logger.Warn(
                        $@"{nameof(Saturn)}|{
                                nameof(GetPriceAsync)
                            }| Price is invalid. Request = ""{request}"", RequestBody = ""{
                                requestBody
                            }"", Response = ""{
                                result
                            }"", From = ""{JsonConverter.SerializeObject(fromGeocoderResult)}"", To = ""{
                                JsonConverter.SerializeObject(toGeocoderResult)
                            }""");
                    return null;
                }

                string estimateStr = price.estimCostList[0].cost.ToString();
                decimal estimateDec;
                if (!decimal.TryParse(estimateStr, out estimateDec))
                {
                    Logger.Warn(
                        $@"{nameof(Saturn)}|{nameof(GetPriceAsync)}| Can't parse price.{nameof(estimateStr)} = ""{
                                estimateStr
                            }"", Request = ""{request}"", RequestBody = ""{requestBody}"", Response = ""{
                                result
                            }"", From = ""{JsonConverter.SerializeObject(fromGeocoderResult)}"", To = ""{
                                JsonConverter.SerializeObject(toGeocoderResult)
                            }""");
                    return null;
                }

                return new TaxiInfo
                {
                    Name = _options.name,
                    Site = _options.site,
                    Price = estimateDec,
                    IsExact = _options.isExact
                };
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(Saturn)}|{nameof(GetPriceAsync)}| RequestBody = ""{requestBody}"", From = ""{
                            JsonConverter.SerializeObject(fromGeocoderResult)
                        }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                return null;
            }
        }

        private async Task<int> GetStreetIdAsync(GeocoderResult geocoderResult)
        {
            var requestBody = new AddressRequest()
            {
                authrec = _options.authrec,
                filtercityid = _options.filtercityid["Новосибирск"],
                filterulicaid = 0,
                limit = 1,
                template = geocoderResult.Address.CleanRoad
            };

            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, _options.streetApi)
                {
                    Content = new StringContent(JsonConverter.SerializeObject(requestBody), Encoding.UTF8,
                        MediaType.Json)
                };
                request.Headers.Add(Headers.UserAgent, _options.userAgent);

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(Saturn)}|{
                                nameof(GetStreetIdAsync)
                            } failed. Request = ""{request}"", RequestBody = ""{requestBody}"", Response = ""{
                                result
                            }"", Address = ""{JsonConverter.SerializeObject(geocoderResult)}""");
                    return 0;
                }

                dynamic address = JsonConverter.DeserializeObject(result);
                if (address == null || address.adrlist == null || address.adrlist.Count == 0 ||
                    address.adrlist[0].ulicaId == null)
                {
                    Logger.Warn(
                        $@"{nameof(Saturn)}|{
                                nameof(GetStreetIdAsync)
                            }| Address is invalid. Request = ""{request}"", RequestBody = ""{
                                requestBody
                            }"", Response = ""{
                                result
                            }"", Address = ""{JsonConverter.SerializeObject(geocoderResult)}""");
                    return 0;
                }

                return address.adrlist[0].ulicaId;
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(Saturn)}|{nameof(GetStreetIdAsync)}| Address = ""{
                            JsonConverter.SerializeObject(geocoderResult)
                        }""");
                return 0;
            }
        }

        private async Task<Address> GetAddressIdAsync(GeocoderResult geocoderResult)
        {
            var streetIdResult = await GetStreetIdAsync(geocoderResult);
            if (streetIdResult == 0)
            {
                Logger.Warn(
                    $@"{nameof(Saturn)}|{nameof(GetAddressIdAsync)}| StreetId is invalid. Address = ""{
                            JsonConverter.SerializeObject(geocoderResult)
                        }""");
                return null;
            }


            var requestBody = new AddressRequest()
            {
                authrec = _options.authrec,
                filtercityid = _options.filtercityid["Новосибирск"],
                filterulicaid = streetIdResult,
                limit = 1,
                template = GetHouseNumber(geocoderResult.Address.GetHouseNumber)
            };

            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, _options.streetApi)
                {
                    Content = new StringContent(JsonConverter.SerializeObject(requestBody), Encoding.UTF8,
                        MediaType.Json)
                };
                request.Headers.Add(Headers.UserAgent, _options.userAgent);

                var response = await _httpClient.SendAsync(request);

                var result = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(Saturn)}|{
                                nameof(GetAddressIdAsync)
                            } failed. Request = ""{request}"", RequestBody = ""{requestBody}"", Response = ""{
                                result
                            }"", Address = ""{JsonConverter.SerializeObject(geocoderResult)}""");
                    return null;
                }

                dynamic address = JsonConverter.DeserializeObject(result);
                if (address == null || address.adrlist == null || address.adrlist.Count == 0
                    || address.adrlist[0].idx == null || address.adrlist[0].type == null)
                {
                    Logger.Warn(
                        $@"{nameof(Saturn)}|{
                                nameof(GetAddressIdAsync)
                            }| Address is invalid. Request = ""{request}"", RequestBody = ""{
                                requestBody
                            }"", Response = ""{
                                result
                            }"", Address = ""{JsonConverter.SerializeObject(geocoderResult)}""");
                    return null;
                }

                return new Address {idx = address.adrlist[0].idx, type = address.adrlist[0].type};
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(Saturn)}|{nameof(GetAddressIdAsync)}| Address = ""{
                            JsonConverter.SerializeObject(geocoderResult)
                        }""");
                return null;
            }
        }

        private static string GetHouseNumber(string houseNumber)
        {
            return char.IsDigit(houseNumber.LastOrDefault())
                ? houseNumber
                : $"{houseNumber.Substring(0, houseNumber.Length - 1)}/{houseNumber.Substring(houseNumber.Length - 1, 1)}";
        }

        private class AddressRequest
        {
            public int filterulicaid { get; set; }
            public int filtercityid { get; set; }
            public SaturnAuth authrec { get; set; }
            public string template { get; set; }
            public int limit { get; set; }
        }

        private class PriceRequest
        {
            public int orderoptionid { get; set; }
            public SaturnAuth authrec { get; set; }
            public List<Address> addressList { get; set; }
        }

        private class Address
        {
            public int idx { get; set; }
            public string type { get; set; }
        }
    }
}