﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;
using NLog;
using TaxisComparer.Infrastructure.Common;
using TaxisComparer.Infrastructure.Common.Cookie;
using TaxisComparer.Infrastructure.Geocoding;
using TaxisComparer.Infrastructure.Options;
using TaxisComparer.Models;
using Cookie = TaxisComparer.Infrastructure.Common.Cookie.Cookie;

namespace TaxisComparer.Infrastructure.Taxis
{
    public class Leader : ITaxi
    {
        public string Name => _options.name;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly LeaderOption _options;
        private readonly HttpClient _httpClient;

        public Leader(IOptions<LeaderOption> options, HttpClient httpClient)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<TaxiInfo> GetPriceAsync(GeocoderResult fromGeocoderResult, GeocoderResult toGeocoderResult)
        {
            var phpAndQip = GetPhpSessIdAndQipAsync();

            var fromStreetId = GetStreetIdAsync(fromGeocoderResult);
            var toStreetId = GetStreetIdAsync(toGeocoderResult);

            var fromStreeIdstr = fromStreetId.Result;
            var toStreetIdstr = toStreetId.Result;

            if (string.IsNullOrEmpty(fromStreeIdstr) || string.IsNullOrEmpty(toStreetIdstr))
            {
                Logger.Warn(
                    $@"{nameof(Leader)}|{nameof(GetPriceAsync)}| Address is null. From = ""{fromStreeIdstr}"", To = ""{
                            toStreetIdstr
                        }""");
                return null;
            }

            fromStreeIdstr = fromStreeIdstr.Replace("|", "-");
            toStreetIdstr = toStreetIdstr.Replace("|", "-");

            var phpsessid = phpAndQip.Result.Item1;
            var qip = phpAndQip.Result.Item2;

            if (string.IsNullOrWhiteSpace(phpsessid?.Value) || string.IsNullOrWhiteSpace(qip))
            {
                Logger.Warn(
                    $@"{nameof(Leader)}|{nameof(GetPriceAsync)}| Qip or PHPSESSID is null or white space. From = ""{
                            phpsessid?.Value
                        }"", To = ""{qip}""");
                return null;
            }

            var requestBody =
                $"p[]=1&p[]=0&p[]=&p[]=&p[]=0&p[]=1&p[]=&p[]=&p[]=&p[]=&p[]={fromStreeIdstr}&p[]={fromGeocoderResult.Address.GetHouseNumber}&p[]=&p[]=&p[]={toStreetIdstr}#${toGeocoderResult.Address.GetHouseNumber}#$&p[]=&p[]=&p[]=&p[]=&p[]=&p[]=&p[]=&p[]=&p[]=&p[]=0&p[]=0&p[]=0&p[]=0&p[]=0&p[]=0&p[]=0&p[]=0&p[]=&p[]=0&p[]=0";
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, QueryHelpers.AddQueryString(_options.priceApi,
                    new Dictionary<string, string>
                    {
                        {nameof(qip), qip}
                    }))
                {
                    Content = new StringContent(requestBody, Encoding.UTF8, MediaType.XWwwFormUrlencoded)
                };
                request.Headers.Clear();
                request.Headers.Add(Headers.XRequestedWith, Headers.XmlHttpRequest);
                request.Headers.Add(Headers.Cookie, phpsessid.ToString());

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(Leader)}|{nameof(GetPriceAsync)} failed. Qip = ""{qip}"", PHPSESSID = ""{
                                phpsessid.Value
                            }"", Request = ""{request}"", RequestBody = ""{requestBody}"", Response = ""{
                                result
                            }"", From = ""{JsonConverter.SerializeObject(fromGeocoderResult)}"", To = ""{
                                JsonConverter.SerializeObject(toGeocoderResult)
                            }""");
                    return null;
                }

                dynamic leaderPrice = JsonConverter.DeserializeObject(result);

                if (leaderPrice == null || leaderPrice.cost == null)
                {
                    Logger.Warn(
                        $@"{nameof(Leader)}|{nameof(GetPriceAsync)}| Price is invalid. Qip = ""{qip}"", PHPSESSID = ""{
                                phpsessid.Value
                            }"", Request = ""{request}"", RequestBody = ""{requestBody}"", Response = ""{
                                result
                            }"", From = ""{JsonConverter.SerializeObject(fromGeocoderResult)}"", To = ""{
                                JsonConverter.SerializeObject(toGeocoderResult)
                            }""");
                    return null;
                }

                string estimateStr = leaderPrice.cost.ToString();
                decimal estimateDec;
                if (!decimal.TryParse(estimateStr, out estimateDec))
                {
                    Logger.Warn(
                        $@"{nameof(Leader)}|{nameof(GetPriceAsync)}| Can't parse price.{nameof(estimateStr)} = ""{
                                estimateStr
                            }"", Request = ""{request}"", RequestBody = ""{requestBody}"", Response = ""{
                                result
                            }"", From = ""{JsonConverter.SerializeObject(fromGeocoderResult)}"", To = ""{
                                JsonConverter.SerializeObject(toGeocoderResult)
                            }""");
                    return null;
                }

                return new TaxiInfo
                {
                    Name = _options.name,
                    Price = estimateDec,
                    Site = _options.site,
                    IsExact = _options.isExact
                };
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(Leader)}|{nameof(GetStreetIdAsync)}|  Qip = ""{qip}"", PHPSESSID = ""{
                            phpsessid.Value
                        }"", RequestBody = {requestBody}, From = ""{
                            JsonConverter.SerializeObject(fromGeocoderResult)
                        }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                return null;
            }
        }

        private async Task<string> GetStreetIdAsync(GeocoderResult geocoderResult)
        {
            var term = geocoderResult.Address.CleanRoad;
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Get,
                    QueryHelpers.AddQueryString(_options.addressApi, new Dictionary<string, string>
                    {
                        {nameof(term), term}
                    }));

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(Leader)}|{nameof(GetStreetIdAsync)} failed. Request = ""{request}"", Response = ""{
                                result
                            }"", Address = ""{JsonConverter.SerializeObject(geocoderResult)}""");
                    return null;
                }

                dynamic address = JsonConverter.DeserializeObject(result);

                if (address == null || address.Count == null || address.Count == 0 || address[0] == null ||
                    address[0].id == null)
                {
                    Logger.Warn(
                        $@"{nameof(Leader)}|{
                                nameof(GetStreetIdAsync)
                            }| Address is invalid. Request = ""{request}"", Response = ""{result}"", Address = ""{
                                JsonConverter.SerializeObject(geocoderResult)
                            }""");
                    return null;
                }

                return address[0].id;
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(Leader)}|{nameof(GetStreetIdAsync)}| Address = ""{
                            JsonConverter.SerializeObject(geocoderResult)
                        }""");
                return null;
            }
        }

        private async Task<Tuple<Cookie, string>> GetPhpSessIdAndQipAsync()
        {
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Get, _options.homeUrl);
                var response = await _httpClient.SendAsync(request);
                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn($@"{nameof(Leader)}|{nameof(GetPhpSessIdAndQipAsync)} failed.");
                    return null;
                }

                var phpSessId = response.GetCookie()
                    .Find(c => c.Name.Equals(Cookies.PhpSessId, StringComparison.OrdinalIgnoreCase));
                if (string.IsNullOrWhiteSpace(phpSessId?.Value))
                {
                    Logger.Warn(
                        $@"{nameof(Leader)}|{nameof(GetPhpSessIdAndQipAsync)}| {
                                Cookies.PhpSessId
                            } is null or white space.");
                    return null;
                }

                var page = response.Content.ReadAsStringAsync().Result;
                if (string.IsNullOrWhiteSpace(page))
                {
                    Logger.Warn($@"{nameof(Leader)}|{nameof(GetPhpSessIdAndQipAsync)}| Page is null or white space.");
                    return null;
                }

                var qipIndexStart = page.IndexOf(_options.qipName, StringComparison.OrdinalIgnoreCase);
                var qipIndexEnd = page.IndexOf(";", qipIndexStart, StringComparison.OrdinalIgnoreCase);
                if (qipIndexEnd == 0 || qipIndexEnd == 0)
                {
                    Logger.Warn($@"{nameof(Leader)}|{nameof(GetPhpSessIdAndQipAsync)}| Qip not found on page.");
                    return null;
                }


                var qipString = page.Substring(qipIndexStart, qipIndexEnd - qipIndexStart);
                var qipParams = qipString.Split('=')
                    .Select(p => p.Trim())
                    .Where(p => !string.IsNullOrWhiteSpace(p))
                    .ToArray();
                if (qipParams.Length < 2)
                {
                    Logger.Warn($@"{nameof(Leader)}|{nameof(GetPhpSessIdAndQipAsync)}| Qip is invalid.");
                    return null;
                }

                var qip = qipParams[1].Trim('"');

                return new Tuple<Cookie, string>(phpSessId, qip);
            }
            catch (Exception exception)
            {
                Logger.Error(exception, $"{nameof(Leader)}|{nameof(GetPhpSessIdAndQipAsync)}");
                return null;
            }
        }
    }
}