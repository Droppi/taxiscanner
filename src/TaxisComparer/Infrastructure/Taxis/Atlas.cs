﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;
using NLog;
using TaxisComparer.Infrastructure.Common;
using TaxisComparer.Infrastructure.Common.Cookie;
using TaxisComparer.Infrastructure.Geocoding;
using TaxisComparer.Infrastructure.Options;
using TaxisComparer.Models;
using Cookie = TaxisComparer.Infrastructure.Common.Cookie.Cookie;

namespace TaxisComparer.Infrastructure.Taxis
{
    public class Atlas : ITaxi
    {
        public string Name => _options.name;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly AtlasOption _options;
        private readonly HttpClient _httpClient;

        public Atlas(IOptions<AtlasOption> options, HttpClient httpClient)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<TaxiInfo> GetPriceAsync(GeocoderResult fromGeocoderResult, GeocoderResult toGeocoderResult)
        {
            var phpsessid = GetPhpSessIdAsync()?.Result;
            if (string.IsNullOrEmpty(phpsessid?.Value))
            {
                Logger.Warn($"{nameof(Atlas)}|{nameof(GetPriceAsync)}| PHPSESSID is null or empty.");
                return null;
            }

            ConfirmPhpSessId(phpsessid);

            var fromStreetId = GetStreetIdAsync(fromGeocoderResult, phpsessid);
            var toStreetId = GetStreetIdAsync(toGeocoderResult, phpsessid);

            var address1 = fromStreetId.Result;
            var address2 = toStreetId.Result;

            if (string.IsNullOrEmpty(address1) || string.IsNullOrEmpty(address2))
            {
                Logger.Warn(
                    $@"{nameof(Atlas)}|{
                            nameof(GetPriceAsync)
                        }| Address is null. From = ""{address1}"", To = ""{address2}""");
                return null;
            }

            var requestBody =
                $"{nameof(address1)}={address1}&{nameof(address2)}={address2}&{nameof(_options.phone)}={_options.phone}&{nameof(_options.channel)}={_options.channel["стандарт"]}&{nameof(_options.date_type)}={_options.date_type}";
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, _options.priceApi)
                {
                    Content = new StringContent(requestBody, Encoding.UTF8, MediaType.XWwwFormUrlencoded)
                };
                request.Headers.Add(Headers.Cookie, phpsessid.ToString());

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(Atlas)}|{nameof(GetPriceAsync)} failed. PHPSESSID = ""{
                                phpsessid.Value
                            }"", Request = ""{request}"", RequestBody = ""{requestBody}"", Response = ""{
                                result
                            }"", From = ""{JsonConverter.SerializeObject(fromGeocoderResult)}"", To = ""{
                                JsonConverter.SerializeObject(toGeocoderResult)
                            }""");
                    return null;
                }

                dynamic atlasPrice = JsonConverter.DeserializeObject(result);

                if (atlasPrice == null || atlasPrice.cost == null)
                {
                    Logger.Warn(
                        $@"{nameof(Atlas)}|{
                                nameof(GetPriceAsync)
                            }| Price is invalid. Request = ""{request}"", RequestBody = ""{
                                requestBody
                            }"", Response = ""{
                                result
                            }"", From = ""{JsonConverter.SerializeObject(fromGeocoderResult)}"", To = ""{
                                JsonConverter.SerializeObject(toGeocoderResult)
                            }""");
                    return null;
                }

                string estimateStr = atlasPrice.cost.ToString();
                decimal estimateDec;
                if (!decimal.TryParse(estimateStr, out estimateDec))
                {
                    Logger.Warn(
                        $@"{nameof(Atlas)}|{nameof(GetPriceAsync)}| Can't parse price.{nameof(estimateStr)} = ""{
                                estimateStr
                            }"", Request = ""{request}"", RequestBody = ""{requestBody}"", Response = ""{
                                result
                            }"", From = ""{JsonConverter.SerializeObject(fromGeocoderResult)}"", To = ""{
                                JsonConverter.SerializeObject(toGeocoderResult)
                            }""");
                    return null;
                }

                return new TaxiInfo
                {
                    Name = _options.name,
                    Price = estimateDec,
                    Site = _options.site,
                    IsExact = _options.isExact
                };
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(Atlas)}|{nameof(GetPriceAsync)}| PHPSESSID = ""{phpsessid.Value}"", RequestBody = {
                            requestBody
                        }, From = {JsonConverter.SerializeObject(fromGeocoderResult)}, To = {
                            JsonConverter.SerializeObject(toGeocoderResult)
                        }");
                return null;
            }
        }

        //Иногда ошибка при втором запросе сразу.
        //Full query 
        //$"q={geocoderResult.Address.CleanRoad}, {geocoderResult.Address.House_Number}&limit={300}&timestamp={Helper.ToTimestamp(DateTime.Now)}&{nameof(city)}={city}&channel=0",
        private async Task<string> GetStreetIdAsync(GeocoderResult geocoderResult, Cookie phpsessid)
        {
            var requestBody =
                $"q={geocoderResult.Address.CleanRoad}, {geocoderResult.Address.GetHouseNumber}&{nameof(_options.city)}={_options.city}";

            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, _options.addressApi)
                {
                    Content = new StringContent(requestBody, Encoding.UTF8, MediaType.XWwwFormUrlencoded)
                };
                request.Headers.Clear();
                request.Headers.Add(Headers.XRequestedWith, Headers.XmlHttpRequest);
                request.Headers.Add(Headers.UserAgent, Headers.UserAgentValue);
                request.Headers.Add(nameof(_options.Referer), _options.Referer);
                request.Headers.Add(Headers.Cookie, phpsessid.ToString());

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(Atlas)}|{nameof(GetStreetIdAsync)} failed. PHPSESSID = ""{
                                phpsessid.Value
                            }"", Request = ""{request}"", RequestBody = ""{requestBody}"", Response = ""{
                                result
                            }"", Address = ""{JsonConverter.SerializeObject(geocoderResult)}""");
                    return null;
                }

                var address = result.Split(new[] {Environment.NewLine}, StringSplitOptions.None)
                    .FirstOrDefault()
                    ?.Split('|');
                if (address == null || address.Length == 0)
                {
                    Logger.Warn(
                        $@"{nameof(Atlas)}|{
                                nameof(GetStreetIdAsync)
                            }| Address not found. Request = ""{request}"", RequestBody = ""{
                                requestBody
                            }"", Response = ""{
                                result
                            }"", Address = ""{JsonConverter.SerializeObject(geocoderResult)}""");
                    return null;
                }

                if (address.Length < 3 || address[2] == null || address[2] != "house")
                {
                    Logger.Warn(
                        $@"{nameof(Atlas)}|{
                                nameof(GetStreetIdAsync)
                            }| Address is invalid. Request = ""{request}"", RequestBody = ""{
                                requestBody
                            }"", Response = ""{
                                result
                            }"", Address = ""{JsonConverter.SerializeObject(geocoderResult)}""");
                    return null;
                }

                return address[1];
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(Atlas)}|{nameof(GetStreetIdAsync)}| PHPSESSID = ""{phpsessid.Value}"", RequestBody = ""{
                            requestBody
                        }"", Address = ""{JsonConverter.SerializeObject(geocoderResult)}""");
                return null;
            }
        }

        private async Task<Cookie> GetPhpSessIdAsync()
        {
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Get, _options.formUrl);
                request.Headers.Clear();
                request.Headers.Add(Headers.UserAgent, Headers.UserAgentValue);

                var response = await _httpClient.SendAsync(request);

                return response.GetCookie()
                    .Find(c => c.Name.Equals(Cookies.PhpSessId, StringComparison.OrdinalIgnoreCase));
            }
            catch (Exception exception)
            {
                Logger.Error(exception, $"{nameof(Atlas)}|{nameof(GetPhpSessIdAsync)}");
                return null;
            }
        }

        private void ConfirmPhpSessId(Cookie phpsessid)
        {
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Get, QueryHelpers.AddQueryString(
                    _options.kcaptchaUrl, new Dictionary<string, string>
                    {
                        {Cookies.PhpSessId, phpsessid.Value}
                    }));
                var response = _httpClient.SendAsync(request).Result;
            }
            catch (Exception exception)
            {
                Logger.Error(exception, $"{nameof(Atlas)}|{nameof(ConfirmPhpSessId)}");
            }
        }
    }
}