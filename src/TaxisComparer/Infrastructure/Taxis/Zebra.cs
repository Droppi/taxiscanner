﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;
using NLog;
using TaxisComparer.Infrastructure.Common;
using TaxisComparer.Infrastructure.Geocoding;
using TaxisComparer.Infrastructure.Options;
using TaxisComparer.Models;

namespace TaxisComparer.Infrastructure.Taxis
{
    public class Zebra : ITaxi
    {
        public string Name => _options.name;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly ZebraOption _options;
        private readonly HttpClient _httpClient;

        public Zebra(IOptions<ZebraOption> options, HttpClient httpClient)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<TaxiInfo> GetPriceAsync(GeocoderResult fromGeocoderResult, GeocoderResult toGeocoderResult)
        {
            var fromStreetId = GetStreetIdAsync(fromGeocoderResult);
            var toStreetId = GetStreetIdAsync(toGeocoderResult);

            var fromAddress = fromStreetId.Result;
            var toAddress = toStreetId.Result;

            if (string.IsNullOrEmpty(fromAddress) || string.IsNullOrEmpty(toAddress))
            {
                Logger.Warn(
                    $@"{nameof(Zebra)}|{nameof(GetPriceAsync)}| Address is null. From = ""{fromAddress}"", To = ""{
                            toAddress
                        }""");
                return null;
            }

            var points = $"[\"{fromAddress}\",\"{toAddress}\"]";

            var requestQuery = QueryHelpers.AddQueryString(_options.priceApi, new Dictionary<string, string>
            {
                {nameof(_options.city_id), _options.city_id.ToString()},
                {nameof(_options.tariff_id), _options.tariff_id.ToString()},
                {nameof(points), points}
            });

            try
            {
                var request = new HttpRequestMessage(HttpMethod.Get, requestQuery);

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(Zebra)}|{nameof(GetPriceAsync)} failed. Request = ""{request}"", Response = ""{
                                result
                            }"", From = ""{JsonConverter.SerializeObject(fromGeocoderResult)}"", To = ""{
                                JsonConverter.SerializeObject(toGeocoderResult)
                            }""");
                    return null;
                }

                dynamic price = JsonConverter.DeserializeObject(result);

                if (price == null || price.response == null)
                {
                    Logger.Warn(
                        $@"{nameof(Zebra)}|{
                                nameof(GetPriceAsync)
                            }| Price is invalid. Request = ""{request}"", Response = ""{result}"", From = ""{
                                JsonConverter.SerializeObject(fromGeocoderResult)
                            }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                    return null;
                }

                string estimateStr = price.response.ToString();
                decimal estimateDec;
                if (!decimal.TryParse(estimateStr, out estimateDec))
                {
                    Logger.Warn(
                        $@"{nameof(Atlas)}|{nameof(GetPriceAsync)}| Can't parse price.{nameof(estimateStr)} = ""{
                                estimateStr
                            }"", Request = ""{request}"", Response = ""{result}"", From = ""{
                                JsonConverter.SerializeObject(fromGeocoderResult)
                            }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                    return null;
                }

                return new TaxiInfo
                {
                    Name = _options.name,
                    Price = estimateDec,
                    Site = _options.site,
                    IsExact = _options.isExact
                };
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(Zebra)}|{nameof(GetPriceAsync)}| RequestQuery = ""{requestQuery}"", From = ""{
                            JsonConverter.SerializeObject(fromGeocoderResult)
                        }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                return null;
            }
        }

        private async Task<string> GetStreetIdAsync(GeocoderResult geocoderResult)
        {
            var keyword =
                $"{geocoderResult.Address.City},{geocoderResult.Address.CleanRoad},{geocoderResult.Address.GetHouseNumber}";
            if (string.IsNullOrEmpty(keyword))
                return null;

            var requestQuery = QueryHelpers.AddQueryString(_options.addressApi, new Dictionary<string, string>
            {
                {nameof(_options.city_id), _options.city_id.ToString()},
                {nameof(keyword), keyword}
            });

            try
            {
                var request = new HttpRequestMessage(HttpMethod.Get, requestQuery);

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(Zebra)}|{nameof(GetStreetIdAsync)} failed. Request = ""{request}"", Response = ""{
                                result
                            }"", Address = ""{JsonConverter.SerializeObject(geocoderResult)}""");
                    return null;
                }

                dynamic address = JsonConverter.DeserializeObject(result);

                if (address == null || address.response == null || address.Count == 0 || address.response[0] == null ||
                    address.response[0].id == null)
                {
                    Logger.Warn(
                        $@"{nameof(Zebra)}|{
                                nameof(GetStreetIdAsync)
                            }| Address is invalid. Request = ""{request}"", Response = ""{result}"", Address = ""{
                                JsonConverter.SerializeObject(geocoderResult)
                            }""");
                    return null;
                }

                return address.response[0].id;
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(Zebra)}|{nameof(GetStreetIdAsync)}| RequestQuery =""{requestQuery}"", Address = ""{
                            JsonConverter.SerializeObject(geocoderResult)
                        }""");
                return null;
            }
        }
    }
}