﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using NLog;
using TaxisComparer.Infrastructure.Common;
using TaxisComparer.Infrastructure.Geocoding;
using TaxisComparer.Infrastructure.Options;
using TaxisComparer.Models;
using JsonConverter = TaxisComparer.Infrastructure.Common.JsonConverter;

namespace TaxisComparer.Infrastructure.Taxis
{
    public class Minitax : ITaxi
    {
        public string Name => _options.name;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly MinitaxOption _options;
        private readonly HttpClient _httpClient;

        public Minitax(IOptions<MinitaxOption> options, HttpClient httpClient)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<TaxiInfo> GetPriceAsync(GeocoderResult fromGeocoderResult, GeocoderResult toGeocoderResult)
        {
            var fromStreetId = GetStreetIdAsync(fromGeocoderResult);
            var toStreetId = GetStreetIdAsync(toGeocoderResult);

            var fromAddress = fromStreetId.Result;
            var toAddress = toStreetId.Result;

            if (string.IsNullOrEmpty(fromAddress) || string.IsNullOrEmpty(toAddress))
            {
                Logger.Warn(
                    $@"{nameof(Minitax)}|{nameof(GetPriceAsync)}| Address is null. From = ""{fromAddress}"", To = ""{
                            toAddress
                        }""");
                return null;
            }

            var requestBody = new MinitaxBodyRequest
            {
                calc_from = fromAddress,
                calc_from_house = fromGeocoderResult.Address.GetHouseNumber,
                calc_to = toAddress,
                calc_to_house = toGeocoderResult.Address.GetHouseNumber,
            }.ToString();

            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, _options.priceApi)
                {
                    Content = new StringContent(requestBody, Encoding.UTF8, MediaType.XWwwFormUrlencoded)
                };

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(Minitax)}|{nameof(GetPriceAsync)} failed. Request = ""{request}"", RequestBody = ""{
                                requestBody
                            }"", Response = ""{result}"", From = ""{
                                JsonConverter.SerializeObject(fromGeocoderResult)
                            }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                    return null;
                }

                var htmlPage = new HtmlDocument();
                htmlPage.LoadHtml(result);

                var cost = htmlPage.GetElementbyId(_options.costElementId);
                if (string.IsNullOrWhiteSpace(cost?.InnerText))
                {
                    Logger.Warn(
                        $@"{nameof(Minitax)}|{
                                nameof(GetPriceAsync)
                            }| Price is invalid. Request = ""{request}"", RequestBody = ""{
                                requestBody
                            }"", Response = ""{
                                result
                            }"", From = ""{JsonConverter.SerializeObject(fromGeocoderResult)}"", To = ""{
                                JsonConverter.SerializeObject(toGeocoderResult)
                            }""");
                    return null;
                }

                string estimateStr = cost.InnerText;
                decimal estimateDec;
                if (!decimal.TryParse(estimateStr, out estimateDec))
                {
                    Logger.Warn(
                        $@"{nameof(Atlas)}|{nameof(GetPriceAsync)}| Can't parse price.{nameof(estimateStr)} = ""{
                                estimateStr
                            }"", Request = ""{request}"", RequestBody = ""{requestBody}"", Response = ""{
                                result
                            }"", From = ""{JsonConverter.SerializeObject(fromGeocoderResult)}"", To = ""{
                                JsonConverter.SerializeObject(toGeocoderResult)
                            }""");
                    return null;
                }

                return new TaxiInfo
                {
                    Name = _options.name,
                    Price = estimateDec,
                    Site = _options.site,
                    IsExact = _options.isExact
                };
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(Minitax)}|{nameof(GetPriceAsync)}| RequestBody = ""{requestBody}"", From = ""{
                            JsonConverter.SerializeObject(fromGeocoderResult)
                        }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                return null;
            }
        }

        private async Task<string> GetStreetIdAsync(GeocoderResult geocoderResult)
        {
            var term = geocoderResult.Address.CleanRoad;
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Get,
                    QueryHelpers.AddQueryString(_options.addressApi, new Dictionary<string, string>
                    {
                        {nameof(term), term},
                    }));

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(Minitax)}|{nameof(GetStreetIdAsync)} failed. Request = ""{request}"", Response = ""{
                                result
                            }"", Address = ""{JsonConverter.SerializeObject(geocoderResult)}""");
                    return null;
                }

                dynamic maximAddress = JsonConvert.DeserializeObject(result);

                if (maximAddress == null || maximAddress.Count == 0 || maximAddress[0] == null ||
                    maximAddress[0].id == null)
                {
                    Logger.Warn(
                        $@"{nameof(Minitax)}|{
                                nameof(GetStreetIdAsync)
                            }| Address is invalid. Request = ""{request}"", Response = ""{result}"", Address = ""{
                                JsonConverter.SerializeObject(geocoderResult)
                            }""");
                    return null;
                }

                return maximAddress[0].id;
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(Minitax)}|{nameof(GetStreetIdAsync)}| Address = ""{
                            JsonConverter.SerializeObject(geocoderResult)
                        }""");
                return null;
            }
        }

        private struct MinitaxBodyRequest
        {
            public string calc_from;
            public string calc_from_house;
            public string calc_to;
            public string calc_to_house;

            public override string ToString()
            {
                return
                    $"{nameof(calc_from)}={calc_from}&{nameof(calc_from_house)}={calc_from_house}&{nameof(calc_to)}={calc_to}&{nameof(calc_to_house)}={calc_to_house}";
            }
        }
    }
}