﻿using System.Threading.Tasks;
using TaxisComparer.Infrastructure.Geocoding;
using TaxisComparer.Models;

namespace TaxisComparer.Infrastructure.Taxis
{
    public interface ITaxi
    {
        string Name { get; }

        Task<TaxiInfo> GetPriceAsync(GeocoderResult fromGeocoderResult, GeocoderResult toGeocoderResult);
    }
}
