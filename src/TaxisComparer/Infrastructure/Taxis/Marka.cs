﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using NLog;
using TaxisComparer.Infrastructure.Common;
using TaxisComparer.Infrastructure.Geocoding;
using TaxisComparer.Infrastructure.Options;
using TaxisComparer.Models;
using JsonConverter = TaxisComparer.Infrastructure.Common.JsonConverter;

namespace TaxisComparer.Infrastructure.Taxis
{
    public class Marka : ITaxi
    {
        public string Name => _options.name;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly MarkaOption _options;
        private readonly HttpClient _httpClient;

        public Marka(IOptions<MarkaOption> options, HttpClient httpClient)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<TaxiInfo> GetPriceAsync(GeocoderResult fromGeocoderResult, GeocoderResult toGeocoderResult)
        {
            var fromStreetId = GetStreetIdAsync(fromGeocoderResult);
            var toStreetId = GetStreetIdAsync(toGeocoderResult);

            var fromAddress = fromStreetId.Result;
            var toAddress = toStreetId.Result;

            if (fromAddress == null || toAddress == null)
            {
                Logger.Warn(
                    $@"{nameof(Marka)}|{nameof(GetPriceAsync)}| Address is null. From = ""{fromAddress}"", To = ""{
                            toAddress
                        }""");
                return null;
            }

            if (string.IsNullOrWhiteSpace(fromGeocoderResult.Address.GetHouseNumber) ||
                string.IsNullOrWhiteSpace(toGeocoderResult.Address.GetHouseNumber))
            {
                Logger.Warn(
                    $@"{nameof(Marka)}|{nameof(GetPriceAsync)}| A some House_Number is null or white space.  From = ""{
                            JsonConverter.SerializeObject(fromGeocoderResult)
                        }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                return null;
            }

            var Action = "GetPrice";
            var FromStreetID = fromAddress.ID;
            var FromRegionID = fromAddress.IDRegion;
            var FromCityID = fromAddress.IDCity;
            var FromHouseNumber = fromGeocoderResult.Address.GetHouseNumber.Escape();

            var ToHouseNumber = toGeocoderResult.Address.GetHouseNumber.Escape();

            var requestBody =
                $"{nameof(Action)}={Action}&{nameof(FromStreetID)}={FromStreetID}&{nameof(FromRegionID)}={FromRegionID}&{nameof(FromCityID)}={FromCityID}&{nameof(FromHouseNumber)}={FromHouseNumber}&{_options.toStreetIDName}={toAddress.ID}&{_options.toRegionIDName}={toAddress.IDRegion}&{_options.toCityIDName}={toAddress.IDCity}&{_options.toHouseNumberName}={ToHouseNumber}";
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, _options.api)
                {
                    Content = new StringContent(requestBody, Encoding.UTF8, MediaType.XWwwFormUrlencoded)
                };

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(Marka)}|{nameof(GetPriceAsync)} failed. Request = ""{request}"", RequestBody = ""{
                                requestBody
                            }"", Response = ""{result}"", From = ""{
                                JsonConverter.SerializeObject(fromGeocoderResult)
                            }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                    return null;
                }

                dynamic markaPrice = JsonConverter.DeserializeObject(result);

                if (markaPrice == null || markaPrice.js == null || markaPrice.js.content == null ||
                    markaPrice.js.content.cost == null)
                {
                    Logger.Warn(
                        $@"{nameof(Marka)}|{
                                nameof(GetPriceAsync)
                            }| Price is invalid. Request = ""{request}"", RequestBody = ""{
                                requestBody
                            }"", Response = ""{
                                result
                            }"", From = ""{JsonConverter.SerializeObject(fromGeocoderResult)}"", To = ""{
                                JsonConverter.SerializeObject(toGeocoderResult)
                            }""");
                    return null;
                }

                string estimateStr = markaPrice.js.content.cost.ToString();
                decimal estimateDec;
                if (!decimal.TryParse(estimateStr, out estimateDec))
                {
                    Logger.Warn(
                        $@"{nameof(Marka)}|{nameof(GetPriceAsync)}| Can't parse price.{nameof(estimateStr)} = ""{
                                estimateStr
                            }"", Request = ""{request}"", RequestBody = ""{requestBody}"", Response = ""{
                                result
                            }"", From = ""{JsonConverter.SerializeObject(fromGeocoderResult)}"", To = ""{
                                JsonConverter.SerializeObject(toGeocoderResult)
                            }""");
                    return null;
                }

                //TODO: Hack. Need to fix: Декабристов 86
                if (estimateDec == 50)
                    return null;

                return new TaxiInfo
                {
                    Name = _options.name,
                    Price = estimateDec,
                    Site = _options.site,
                    IsExact = _options.isExact
                };
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(Marka)}|{nameof(GetPriceAsync)}| RequestBody = ""{requestBody}"", From = ""{
                            JsonConverter.SerializeObject(fromGeocoderResult)
                        }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                return null;
            }
        }

        private async Task<dynamic> GetStreetIdAsync(GeocoderResult geocoderResult)
        {
            var Action = "GetStreetList";

            if (string.IsNullOrWhiteSpace(geocoderResult.Address.CleanRoad))
            {
                Logger.Warn(
                    $@"{nameof(Minitax)}|{nameof(GetStreetIdAsync)}| CleanRoad is null or white space. Address = ""{
                            JsonConverter.SerializeObject(geocoderResult)
                        }""");
                return null;
            }

            var Street = geocoderResult.Address.CleanRoad.Escape();

            var requestBody = $"{nameof(Action)}={Action}&{nameof(Street)}={Street}";
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, _options.api)
                {
                    Content = new StringContent(requestBody, Encoding.UTF8, MediaType.OctetStream)
                };

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(Marka)}|{nameof(GetStreetIdAsync)} failed. Request = ""{request}"", RequestBody = ""{
                                requestBody
                            }"", Response = ""{result}"", Address = ""{
                                JsonConverter.SerializeObject(geocoderResult)
                            }""");
                    return null;
                }

                dynamic markaAddress = JsonConvert.DeserializeObject(result);

                if (markaAddress == null || markaAddress.js == null || markaAddress.js.StreetList == null ||
                    markaAddress.js.StreetList.Count == 0)
                {
                    Logger.Warn(
                        $@"{nameof(Marka)}|{
                                nameof(GetStreetIdAsync)
                            }| Address is invalid. Request = ""{request}"", RequestBody = ""{
                                requestBody
                            }"", Response = ""{
                                result
                            }"", Address = ""{JsonConverter.SerializeObject(geocoderResult)}""");
                    return null;
                }

                return markaAddress.js.StreetList[0];
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(Minitax)}|{nameof(GetStreetIdAsync)}| Address = ""{
                            JsonConverter.SerializeObject(geocoderResult)
                        }""");
                return null;
            }
        }
    }
}