﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using NLog;
using TaxisComparer.Infrastructure.Common;
using TaxisComparer.Infrastructure.Geocoding;
using TaxisComparer.Infrastructure.Options;
using TaxisComparer.Models;

namespace TaxisComparer.Infrastructure.Taxis
{
    public class Yandex : ITaxi
    {
        public string Name => _options.name;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly YandexOption _options;
        private readonly HttpClient _httpClient;

        public Yandex(IOptions<YandexOption> options, HttpClient httpClient)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<TaxiInfo> GetPriceAsync(GeocoderResult fromGeocoderResult, GeocoderResult toGeocoderResult)
        {
            var requestBody = new YandexBodyRequest
            {
                city = _options.city,
                zone_name = _options.zone_name,
                supports_forced_surge = _options.supports_forced_surge,
                skip_estimated_waiting = _options.skip_estimated_waiting,
                route = new[,]
                {
                    {Math.Round(fromGeocoderResult.Lon, 6), Math.Round(fromGeocoderResult.Lat, 6)},
                    {Math.Round(toGeocoderResult.Lon, 6), Math.Round(toGeocoderResult.Lat, 6)}
                }
            };

            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, _options.priceApi)
                {
                    Content = new StringContent(JsonConverter.SerializeObject(requestBody), Encoding.UTF8,
                        MediaType.Json)
                };

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(Yandex)}|{nameof(GetPriceAsync)} failed. Request = ""{request}"", RequestBody = ""{
                                JsonConverter.SerializeObject(requestBody)
                            }"", Response = ""{result}"", From = ""{
                                JsonConverter.SerializeObject(fromGeocoderResult)
                            }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                    return null;
                }

                var yandexResponse = JsonConverter.DeserializeObject<YandexResponse>(result);

                if (yandexResponse?.service_levels == null || yandexResponse.service_levels.Count == 0 ||
                    string.IsNullOrEmpty(yandexResponse.service_levels[0]?.price))
                {
                    Logger.Warn(
                        $@"{nameof(Yandex)}|{
                                nameof(GetPriceAsync)
                            }| Price is invalid. Request = ""{request}"", RequestBody = ""{
                                JsonConverter.SerializeObject(requestBody)
                            }"", Response = ""{result}"", From = ""{
                                JsonConverter.SerializeObject(fromGeocoderResult)
                            }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                    return null;
                }

                var firstPrice = yandexResponse.service_levels.First();

                var estimateStr = firstPrice.price.TrimEnd("руб.").Trim();
                decimal estimateDec;
                if (!decimal.TryParse(estimateStr, out estimateDec))
                {
                    Logger.Warn(
                        $@"{nameof(Uber)}|{nameof(GetPriceAsync)}| Can't parse price. {nameof(firstPrice.price)} = ""{
                                firstPrice.price
                            }"", {nameof(estimateStr)} = ""{estimateStr}"", Request = ""{request}"", RequestBody = ""{
                                JsonConverter.SerializeObject(requestBody)
                            }"", Response = ""{result}"", From = ""{
                                JsonConverter.SerializeObject(fromGeocoderResult)
                            }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                    return null;
                }

                return new TaxiInfo
                {
                    Name = _options.name,
                    Price = estimateDec,
                    Site = _options.site,
                    IsExact = _options.isExact
                };
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(Yandex)}|{nameof(GetPriceAsync)}| RequestBody = ""{
                            JsonConverter.SerializeObject(requestBody)
                        }"", From = ""{JsonConverter.SerializeObject(fromGeocoderResult)}"", To = ""{
                            JsonConverter.SerializeObject(toGeocoderResult)
                        }""");
                return null;
            }
        }

        private struct YandexBodyRequest
        {
            public string city;
            public string zone_name;
            public bool supports_forced_surge;
            public bool skip_estimated_waiting;
            public double[,] route;
        }

        private class YandexResponse
        {
            public List<YandexServiceLevel> service_levels;
        }

        private class YandexServiceLevel
        {
            public string name { get; set; }
            public string price { get; set; }
            public YandexForcedSurge forced_surge { get; set; }
        }

        private class YandexForcedSurge
        {
            public string comment { get; set; }
            public string description { get; set; }
            public decimal value { get; set; }
            public string reason { get; set; }
        }
    }
}