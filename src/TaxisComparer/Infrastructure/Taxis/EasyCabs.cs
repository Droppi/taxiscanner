﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;
using NLog;
using TaxisComparer.Infrastructure.Common;
using TaxisComparer.Infrastructure.Geocoding;
using TaxisComparer.Infrastructure.Options;
using TaxisComparer.Models;

namespace TaxisComparer.Infrastructure.Taxis
{
    public class EasyCabs : ITaxi
    {
        public string Name => _options.name;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly EasyCabsOption _options;
        private readonly HttpClient _httpClient;

        public EasyCabs(IOptions<EasyCabsOption> options, HttpClient httpClient)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<TaxiInfo> GetPriceAsync(GeocoderResult fromGeocoderResult, GeocoderResult toGeocoderResult)
        {
            var lat1 = fromGeocoderResult.Lat;
            var lon1 = fromGeocoderResult.Lon;
            var address1 = $"{fromGeocoderResult.Address.Road}, {fromGeocoderResult.Address.GetHouseNumber}";
            var lat2 = toGeocoderResult.Lat;
            var lon2 = toGeocoderResult.Lon;
            var address2 = $"{toGeocoderResult.Address.Road}, {toGeocoderResult.Address.GetHouseNumber}";

            var requestQuery = QueryHelpers.AddQueryString(_options.priceApi, new Dictionary<string, string>
            {
                {nameof(_options.token), _options.token},
                {nameof(_options.tariffid), _options.tariffid["Экспресс"]},
                {nameof(lat1), lat1.ToString(CultureInfo.InvariantCulture)},
                {nameof(lon1), lon1.ToString(CultureInfo.InvariantCulture)},
                {nameof(address1), address1},
                {nameof(lat2), lat2.ToString(CultureInfo.InvariantCulture)},
                {nameof(lon2), lon2.ToString(CultureInfo.InvariantCulture)},
                {nameof(address2), address2},
            });

            try
            {
                var request = new HttpRequestMessage(HttpMethod.Get, requestQuery);
                request.Headers.Add(Headers.UserAgent, _options.userAgent);

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Warn(
                        $@"{nameof(EasyCabs)}|{nameof(GetPriceAsync)} failed. Request = ""{request}"", Response = ""{
                                result
                            }"", From = ""{JsonConverter.SerializeObject(fromGeocoderResult)}"", To = ""{
                                JsonConverter.SerializeObject(toGeocoderResult)
                            }""");
                    return null;
                }

                dynamic price = JsonConverter.DeserializeObject(result);

                if (price.Error != null && price.Error != 0)
                {
                    Logger.Warn(
                        $@"{nameof(EasyCabs)}|{
                                nameof(GetPriceAsync)
                            }| Price is invalid. Request = ""{request}"", Response = ""{result}"", From = ""{
                                JsonConverter.SerializeObject(fromGeocoderResult)
                            }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                    return null;
                }

                if (price == null || price.Price == null || price.IsPriceFixed == null)
                {
                    Logger.Warn(
                        $@"{nameof(EasyCabs)}|{
                                nameof(GetPriceAsync)
                            }| Price is invalid. Request = ""{request}"", Response = ""{result}"", From = ""{
                                JsonConverter.SerializeObject(fromGeocoderResult)
                            }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                    return null;
                }

                var estimateStr = price.Price.ToString();
                decimal estimateDec;
                if (!decimal.TryParse(estimateStr, out estimateDec))
                {
                    Logger.Warn(
                        $@"{nameof(EasyCabs)}|{nameof(GetPriceAsync)}| Can't parse price. {nameof(estimateStr)} = ""{
                                estimateStr
                            }"", Request = ""{request}"", Response = ""{result}"", From = ""{
                                JsonConverter.SerializeObject(fromGeocoderResult)
                            }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                    return null;
                }

                var isExactStr = price.IsPriceFixed.ToString();
                bool isExact;
                if (!bool.TryParse(isExactStr, out isExact))
                {
                    Logger.Warn(
                        $@"{nameof(EasyCabs)}|{
                                nameof(GetPriceAsync)
                            }| Can't parse IsPriceFixed. {nameof(estimateStr)} = ""{estimateStr}"", Request = ""{
                                request
                            }"", Response = ""{result}"", From = ""{
                                JsonConverter.SerializeObject(fromGeocoderResult)
                            }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                    return null;
                }

                estimateDec = Math.Round(estimateDec);
                if (estimateDec == 0)
                {
                    Logger.Warn(
                        $@"{nameof(EasyCabs)}|{nameof(GetPriceAsync)}| Price is zero. {nameof(estimateStr)} = ""{
                                estimateStr
                            }"", Request = ""{request}"", Response = ""{result}"", From = ""{
                                JsonConverter.SerializeObject(fromGeocoderResult)
                            }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                    return null;
                }

                return new TaxiInfo
                {
                    Name = _options.name,
                    Price = estimateDec,
                    Site = _options.site,
                    IsExact = isExact
                };
            }
            catch (Exception exception)
            {
                Logger.Error(exception,
                    $@"{nameof(EasyCabs)}|{nameof(GetPriceAsync)}| Request = ""{requestQuery}"", From = ""{
                            JsonConverter.SerializeObject(fromGeocoderResult)
                        }"", To = ""{JsonConverter.SerializeObject(toGeocoderResult)}""");
                return null;
            }
        }
    }
}