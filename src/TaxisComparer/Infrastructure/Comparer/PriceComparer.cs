﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NLog;
using TaxisComparer.Infrastructure.Common;
using TaxisComparer.Infrastructure.Geocoding;
using TaxisComparer.Infrastructure.Taxis;
using TaxisComparer.Models;

namespace TaxisComparer.Infrastructure.Comparer
{
    public interface IPriceComparer
    {
        Result<List<TaxiInfo>> GetPriceComparisonAsync(string from, string to);
        Result<List<TaxiInfo>> GetPriceComparisonAsync(Point from, Point to);
        Task<Result<List<TaxiInfo>>> GetPriceComparisonAsync(GeocoderResult from, GeocoderResult to);
        Task GetPriceComparisonAsync(Point @from, Point to, Action<TaxiInfo> callback);
        Task GetPriceComparisonAsync(GeocoderResult @from, GeocoderResult to, Action<TaxiInfo> callback);
    }

    public class PriceComparer : IPriceComparer
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public IServiceProvider ServiceProvider { get; }

        private IGeocoding Geocoding { get; }

        private IConfigurationRoot Configuration { get; }


        public PriceComparer(IServiceProvider serviceProvider, [FromServices]IGeocoding geocoding, IConfigurationRoot configuration)
        {
            if (serviceProvider == null) throw new ArgumentNullException(nameof(serviceProvider));
            if (geocoding == null) throw new ArgumentNullException(nameof(geocoding));
            if (configuration == null) throw new ArgumentNullException(nameof(configuration));

            ServiceProvider = serviceProvider;
            Geocoding = geocoding;
            Configuration = configuration;
        }

        public Result<List<TaxiInfo>> GetPriceComparisonAsync(Point from, Point to)
        {
            if (from == null) throw new ArgumentNullException(nameof(from));
            if (to == null) throw new ArgumentNullException(nameof(to));

            var fromGeocoderResult = Geocoding.ReverseGeocodingAsync(from.Lat, from.Lon);
            var toGeocoderResult = Geocoding.ReverseGeocodingAsync(to.Lat, to.Lon);

            if (fromGeocoderResult?.Result == null)
            {
                return Result.Fail<List<TaxiInfo>>(null, 1, "Не найден начальный адрес!");
            }
            if (toGeocoderResult?.Result == null)
            {
                return Result.Fail<List<TaxiInfo>>(null, 1, "Не найден конечный адрес!");
            }

            return GetPriceComparisonAsync(fromGeocoderResult.Result, toGeocoderResult.Result).Result;
        }

        public Result<List<TaxiInfo>> GetPriceComparisonAsync(string from, string to)
        {
            if (string.IsNullOrWhiteSpace(from)) throw new ArgumentException("Value cannot be null or whitespace.", nameof(from));
            if (string.IsNullOrWhiteSpace(to)) throw new ArgumentException("Value cannot be null or whitespace.", nameof(to));

            var fromAddressTask = Geocoding.Search(from, 1);
            var toAddressTask = Geocoding.Search(to, 1);

            if (fromAddressTask?.Result == null || !fromAddressTask.Result.Any())
            {
                return Result.Fail<List<TaxiInfo>>(null, 1, "Не найден начальный адрес!");
            }
            if (toAddressTask?.Result == null || !toAddressTask.Result.Any())
            {
                return Result.Fail<List<TaxiInfo>>(null, 1, "Не найден конечный адрес!");
            }

            return GetPriceComparisonAsync(fromAddressTask.Result.FirstOrDefault(), toAddressTask.Result.FirstOrDefault()).Result;
        }

        public async Task<Result<List<TaxiInfo>>> GetPriceComparisonAsync(GeocoderResult from, GeocoderResult to)
        {
            var taxis = GetTaxis();

            var stopwatch = Stopwatch.StartNew();
            var prices = (await Task.WhenAll(taxis.Select(t => t.GetPriceAsync(from, to)).ToList())).Where(p => p != null).OrderBy(p => p.Price).ToList();
            stopwatch.Stop();

            Logger.Info($"{nameof(PriceComparer)}|{nameof(GetPriceComparisonAsync)}|From = ({from.Lat}, {from.Lon}), To = ({to.Lat}, {to.Lon}) |Taxis={prices.Count}, Duration{stopwatch.Elapsed}");

            return Result.Success(prices);
        }

        public async Task GetPriceComparisonAsync(Point @from, Point to, Action<TaxiInfo> callback)
        {
            if (from == null) throw new ArgumentNullException(nameof(from));
            if (to == null) throw new ArgumentNullException(nameof(to));

            var fromGeocoderResult = Geocoding.ReverseGeocodingAsync(from.Lat, from.Lon);
            var toGeocoderResult = Geocoding.ReverseGeocodingAsync(to.Lat, to.Lon);

            if (fromGeocoderResult?.Result == null)
            {
                throw new InvalidOperationException("Не найден начальный адрес!");
            }
            if (toGeocoderResult?.Result == null)
            {
                throw new InvalidOperationException("Не найден конечный адрес!");
            }

            await GetPriceComparisonAsync(fromGeocoderResult.Result, toGeocoderResult.Result, callback);
        }

        public async Task GetPriceComparisonAsync(GeocoderResult @from, GeocoderResult to, Action<TaxiInfo> callback)
        {
            var taxis = GetTaxis();

            var count = 0;
            var stopwatch = Stopwatch.StartNew();

            await Task.WhenAll(taxis.Select(t => t.GetPriceAsync(from, to).ContinueWith(task =>
            {
                var taxiInfo = task.Result;
                if (taxiInfo == null)
                    return;

                count++;
                callback(taxiInfo);
            })));

            stopwatch.Stop();

            Logger.Info($"{nameof(PriceComparer)}|{nameof(GetPriceComparisonAsync)}with callback|From = ({from.Lat}, {from.Lon}), To = ({to.Lat}, {to.Lon}) |Taxis={count}, Duration{stopwatch.Elapsed}");
        }

        private IEnumerable<ITaxi> GetTaxis()
        {
            var excludedTaxis = Configuration["ExcludedTaxis"].Split(';');
            return ServiceProvider.GetServices(typeof(ITaxi)).OfType<ITaxi>().Where(t => !excludedTaxis.Contains(t.Name, StringComparer.OrdinalIgnoreCase));
        }
    }
}
