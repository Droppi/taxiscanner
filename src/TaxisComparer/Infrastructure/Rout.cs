﻿namespace TaxisComparer.Infrastructure
{
    public class Rout
    {
        public Point From { get; set; }
        public Point To { get; set; }

        public Rout(double fromLat, double fromLng, double toLat, double toLng)
        {
            From = new Point(fromLat, fromLng);
            To = new Point(toLat, toLng);
        }
    }

    public class Point
    {
        public double Lat { get; set; }
        public double Lon { get; set; }

        public Point(double lat, double lon)
        {
            Lat = lat;
            Lon = lon;
        }
    }
}
