﻿namespace TaxisComparer.Infrastructure.Options
{
    public class GlobaxiOption : TaxiOption
    {
        public string priceApi { get; set; }
        public string addressApi { get; set; }

        public string auth { get; set; }
    }
}
