﻿using System.Collections.Generic;

namespace TaxisComparer.Infrastructure.Options
{
    public class SaturnOption : TaxiOption
    {
        public string priceApi { get; set; }
        public string streetApi { get; set; }

        public string userAgent { get; set; }
        public Dictionary<string, int> filtercityid { get; set; }
        public Dictionary<string, int> orderoptionid { get; set; }

        public SaturnAuth authrec { get; set; }
    }

    public class SaturnAuth
    {
        public string phone { get; set; }
        public string account { get; set; }
        public string deviceid { get; set; }
        public string devicemodel { get; set; }
        public string pincode { get; set; }
        public int configid { get; set; }
    }
}
