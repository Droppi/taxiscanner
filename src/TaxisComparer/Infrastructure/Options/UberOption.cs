﻿namespace TaxisComparer.Infrastructure.Options
{
    public class UberOption : TaxiOption
    {
        public string priceApi { get; set; }

        public string server_token { get; set; }
    }
}
