﻿namespace TaxisComparer.Infrastructure.Options
{
    public class TaxiOption
    {
        public string name { get; set; }

        public string site { get; set; }

        public bool isExact { get; set; }
    }
}
