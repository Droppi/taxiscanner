﻿namespace TaxisComparer.Infrastructure.Options
{
    public class ZebraOption : TaxiOption
    {
        public string priceApi { get; set; }
        public string addressApi { get; set; }

        public int city_id { get; set; }
        public int tariff_id { get; set; }
    }
}
