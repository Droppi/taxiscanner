﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TaxisComparer.Infrastructure.Taxis;

namespace TaxisComparer.Infrastructure.Options
{
    public static class OptionsHelper
    {
        public static IServiceCollection AddTaxiOptions(this IServiceCollection services, IConfigurationRoot configuration)
        {
            services.Configure<AtlasOption>(configuration.GetSection(nameof(Atlas)));
            services.Configure<GlobaxiOption>(configuration.GetSection(nameof(Globaxi)));
            services.Configure<GrandOption>(configuration.GetSection(nameof(Grand)));
            services.Configure<LeaderOption>(configuration.GetSection(nameof(Leader)));
            services.Configure<MarkaOption>(configuration.GetSection(nameof(Marka)));
            services.Configure<MaximOption>(configuration.GetSection(nameof(Maxim)));
            services.Configure<MinitaxOption>(configuration.GetSection(nameof(Minitax)));
            services.Configure<MoiGorodOption>(configuration.GetSection(nameof(MoiGorod)));
            services.Configure<UberOption>(configuration.GetSection(nameof(Uber)));
            services.Configure<YandexOption>(configuration.GetSection(nameof(Yandex)));
            services.Configure<ZebraOption>(configuration.GetSection(nameof(Zebra)));
            services.Configure<EasyCabsOption>(configuration.GetSection(nameof(EasyCabs)));
            services.Configure<SaturnOption>(configuration.GetSection(nameof(Saturn)));
            return services;
        }
    }
}
