﻿namespace TaxisComparer.Infrastructure.Options
{
    public class MinitaxOption : TaxiOption
    {
        public string priceApi { get; set; }
        public string addressApi { get; set; }

        public string costElementId { get; set; }
    }
}
