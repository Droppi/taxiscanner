﻿namespace TaxisComparer.Infrastructure.Options
{
    public class MaximOption : TaxiOption
    {
        public string api { get; set; }
        public string routeApi { get; set; }
        public string calculateService { get; set; }
        public string addressesExService { get; set; }

        public string queryName { get; set; }
        public string rName { get; set; }
        public string targetName { get; set; }
        public string locale { get; set; }
        public int city { get; set; }
    }
}
