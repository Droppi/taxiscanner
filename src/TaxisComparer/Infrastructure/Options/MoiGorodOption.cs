﻿using System.Collections.Generic;

namespace TaxisComparer.Infrastructure.Options
{
    public class MoiGorodOption : TaxiOption
    {
        public string priceApi { get; set; }

        public Dictionary<string, int> CITY_USER { get; set; }
        public Dictionary<string, int> tariffGroupId { get; set; }
    }
}
