﻿using System.Collections.Generic;

namespace TaxisComparer.Infrastructure.Options
{
    public class AtlasOption : TaxiOption
    {
        public string formUrl { get; set; }
        public string kcaptchaUrl { get; set; }
        public string priceApi { get; set; }
        public string addressApi { get; set; }

        public string city { get; set; }
        public string phone { get; set; }
        public string date_type { get; set; }

        public string Referer { get; set; }
        public Dictionary<string, int> channel { get; set; }
    }
}
