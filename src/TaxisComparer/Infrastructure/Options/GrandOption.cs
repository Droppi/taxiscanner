﻿using System.Collections.Generic;

namespace TaxisComparer.Infrastructure.Options
{
    public class GrandOption : TaxiOption
    {
        public string priceApi { get; set; }
        public string addressApi { get; set; }

        public bool ignoreBranch { get; set; }
        public bool NearestTime { get; set; }
        //Car's type
        public Dictionary<string, string> VehicleType { get; set; }
        //Rate's type
        public Dictionary<string, string> VehicleClass { get; set; }

        public string destinationPlacesAddressIdName { get; set; }
        public string destinationPlacesLatitudeName { get; set; }
        public string destinationPlacesLongitudeName { get; set; }
    }
}
