﻿namespace TaxisComparer.Infrastructure.Options
{
    public class MarkaOption : TaxiOption
    {
        public string api { get; set; }

        public string toStreetIDName { get; set; }
        public string toRegionIDName { get; set; }
        public string toCityIDName { get; set; }
        public string toHouseNumberName { get; set; }
    }
}
