﻿using System.Collections.Generic;

namespace TaxisComparer.Infrastructure.Options
{
    public class EasyCabsOption : TaxiOption
    {
        public string priceApi { get; set; }
        public string userAgent { get; set; }

        public string token { get; set; }
        public Dictionary<string, string> tariffid { get; set; }
    }
}
