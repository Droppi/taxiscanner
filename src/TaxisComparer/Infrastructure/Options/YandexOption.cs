﻿namespace TaxisComparer.Infrastructure.Options
{
    public class YandexOption : TaxiOption
    {
        public string priceApi { get; set; }

        public string city { get; set; }

        public string zone_name { get; set; }

        public bool supports_forced_surge { get; set; }

        public bool skip_estimated_waiting { get; set; }
    }
}
