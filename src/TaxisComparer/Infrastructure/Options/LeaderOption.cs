﻿namespace TaxisComparer.Infrastructure.Options
{
    public class LeaderOption : TaxiOption
    {
        public string homeUrl { get; set; }
        public string priceApi { get; set; }
        public string addressApi { get; set; }

        public string qipName { get; set; }
    }
}
