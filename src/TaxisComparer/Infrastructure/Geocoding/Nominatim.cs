﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.WebUtilities;
using NLog;
using TaxisComparer.Infrastructure.Common;

namespace TaxisComparer.Infrastructure.Geocoding
{
    public class Nominatim : IGeocoding
    {
        private readonly HttpClient _httpClient;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private const string format = "json";
        private const string acceptlanguageName = "accept-language";
        private const string acceptlanguage = "ru";
        private const int addressdetails = 1;
        private const string email = "postmaster@taxiscanner.pro";
        private const int extratags = 0;
        private const int namedetails = 0;

        private const string SearchNominatimUrl = "http://nominatim.openstreetmap.org/search";
        private const string countrycodes = "ru";
        private const string viewbox = "82.41119,54.68018,83.68423,55.23216";
        private const int bounded = 1;
        private const int polygon = 0;

        private const string ReverseNominatimUrl = "http://nominatim.openstreetmap.org/reverse";
        private const int zoom = 18;

        public Nominatim(HttpClient httpClient)
        {
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<IList<GeocoderResult>> Search(string q, int limit)
        {
            var query = QueryHelpers.AddQueryString(SearchNominatimUrl, new Dictionary<string, string>
            {
                {nameof(format), format},
                {acceptlanguageName, acceptlanguage},
                {nameof(q), q},
                {nameof(countrycodes), countrycodes},
                {nameof(viewbox), viewbox},
                {nameof(bounded), bounded.ToString()},
                {nameof(polygon), polygon.ToString()},
                {nameof(addressdetails), addressdetails.ToString()},
                {nameof(email), email},
                {nameof(limit), limit.ToString()},
                {nameof(extratags), extratags.ToString()},
                {nameof(namedetails), namedetails.ToString()}
            });

            try
            {
                var request = new HttpRequestMessage(HttpMethod.Get, query);

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Error($@"{nameof(Nominatim)}|{nameof(Search)} failed. Query = ""{query}""");
                    return null;
                }

                return JsonConverter.DeserializeObject<IList<GeocoderResult>>(result);
            }
            catch (WebException webException)
            {
                Logger.Error(webException, query);
                return null;
            }
            catch (Exception exception)
            {
                Logger.Error(exception, query);
                return null;
            }
        }

        public async Task<GeocoderResult> ReverseGeocodingAsync(double lat, double lon)
        {
            var query = QueryHelpers.AddQueryString(ReverseNominatimUrl, new Dictionary<string, string>
            {
                {nameof(format), format},
                {acceptlanguageName, acceptlanguage},
                {nameof(lat), lat.ToString(CultureInfo.InvariantCulture)},
                {nameof(lon), lon.ToString(CultureInfo.InvariantCulture)},
                {nameof(zoom), zoom.ToString()},
                {nameof(addressdetails), addressdetails.ToString()},
                {nameof(email), email},
                {nameof(extratags), extratags.ToString()},
                {nameof(namedetails), namedetails.ToString()}
            });

            try
            {
                var request = new HttpRequestMessage(HttpMethod.Get, query);

                var response = await _httpClient.SendAsync(request);
                var result = response.Content.ReadAsStringAsync().Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Error(
                        $@"{nameof(Nominatim)}|{nameof(ReverseGeocodingAsync)} failed. Query = ""{query}""");
                    return null;
                }

                return JsonConverter.DeserializeObject<GeocoderResult>(result);
            }
            catch (WebException webException)
            {
                Logger.Error(webException, query);
                return null;
            }
            catch (Exception exception)
            {
                Logger.Error(exception, query);
                return null;
            }
        }

        public dynamic AddressLookup(dynamic parameters)
        {
            throw new System.NotImplementedException();
        }
    }
}