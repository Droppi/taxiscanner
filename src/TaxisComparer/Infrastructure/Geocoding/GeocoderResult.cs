﻿using TaxisComparer.Infrastructure.Common;

namespace TaxisComparer.Infrastructure.Geocoding
{
    public class GeocoderResult
    {
        public long Place_Id { get; set; }
        public string Licence { get; set; }
        public string Osm_Type { get; set; }
        public long Osm_Id { get; set; }
        public double Lat { get; set; }
        public double Lon { get; set; }
        public string Display_Name { get; set; }
        public Address Address { get; set; }
        public double[] Boundingbox { get; set; }
    }

    public class Address
    {
        private static readonly string[] ExtraWords = { "улица", "бульвар", "площадь" };

        public string Building { get; set; }
        public string House_Number { get; set; }

        private string _road;
        public string Road
        {
            get { return _road; }
            set
            {
                _road = value;

                var cleanRoad = value;
                foreach (var word in ExtraWords)
                {
                    cleanRoad = cleanRoad.TrimStart(word).TrimEnd(word).Trim();
                }
                CleanRoad = cleanRoad;
            }
        }

        public string Suburb { get; set; }
        public string Hamlet { get; set; }
        public string State { get; set; }
        public string State_District { get; set; }
        public string County { get; set; }
        public string City { get; set; }
        public string City_District { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string Country_Code { get; set; }

        public string CleanRoad { get; private set; }

        public string GetHouseNumber => !string.IsNullOrWhiteSpace(House_Number) ? House_Number : Building;
    }
}
