﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TaxisComparer.Infrastructure.Geocoding
{
    public interface IGeocoding
    {
        Task<IList<GeocoderResult>> Search(string address, int limit);
        Task<GeocoderResult> ReverseGeocodingAsync(double lat, double lng);
        dynamic AddressLookup(dynamic parameters);
    }
}
