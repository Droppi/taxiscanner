﻿; (function () {
    "use strict";

    class StoredAddresses {

        constructor() {
            this.readAddressesFromCookie();
            this.validateCookie();
        }

        readAddressesFromCookie() {
            let _addresses = localStorage.getItem("addresses");

            try {
                this.addresses = _addresses ? JSON.parse(_addresses) : [];
            } catch (e) {
                this.addresses = [];
            }
        }

        validateCookie() {
            if (this.addresses.length >= 6) {
                this.addresses.splice(6, this.addresses.length - 1);
            }

            for (let i = 0; i < this.addresses.length; i++) {
                if (!validateAddress(this.addresses[i])) {
                    this.addresses.splice(i, 1);
                }
            }
        }

        addAddress(data) {
            let value = getValueFromAddress(data);

            if (!value) {
                return;
            }

            let isDuplicate = false;
            for (let j = 0; j < this.addresses.length; j++) {
                if (this.addresses[j].value == value) {
                    isDuplicate = true;
                    break;
                }
            }

            if (isDuplicate) {
                return;
            }

            if (this.addresses.length >= 6) {
                this.addresses.pop();
            }

            this.addresses.unshift({
                value: value,
                data: {
                    lat: data.lat,
                    lon: data.lon,
                    address: {
                        house_number: data.address.house_number,
                        road: data.address.road,
                        city: data.address.city,
                        town: data.address.town
                    }
                }
            });
        }

        saveAddresses() {
            localStorage.setItem("addresses", JSON.stringify(this.addresses));
        }
    }
    
    window.createStoredAddresses = function () {
        window.storedAddresses = new StoredAddresses();
    }
}());

$(window)
    .unload(() => {
        try {
            window.storedAddresses.saveAddresses();
        } catch (e) {
            window.logger.error("Can not saved addresses to cookie", e);
        }
    });