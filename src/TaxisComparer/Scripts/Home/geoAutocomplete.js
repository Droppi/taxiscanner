﻿;(function () {
    "use strict";

    class GeoAutocomplete {
        constructor(input, selectFn) {
            this.input = input;
             this.input.autocomplete({
                lookup: this.addressSource,
                onSelect: (suggestion) => this.addressSelect(suggestion, selectFn),
                minChars: 0,
                deferRequestBy: 300,
                lookupLimit: 7,
                beforeRender: this.beforeRender,
                autoSelectFirst: true
             });
             this.autocomplete = this.input.autocomplete();

             this.input.change(() => {
                 if (this.autocomplete.selectedIndex == -1) {
                     this.input.val("");
                 }
             });

             this.input.blur(() => {
                 if (this.autocomplete.selectedIndex == 0 && this.autocomplete.suggestions.length >= 1) {
                    this.autocomplete.select(0);
                }
            });
        }

        addressSource(query, done) {
            let search_result = [];
            let ru_query = auto_layout_keyboard(query).trim();

            if (ru_query.length == 0) {
                if (window.storedAddresses.addresses) {
                    done({ suggestions: window.storedAddresses.addresses});
                }
            }
            else {
                window.geocoding.findAddress(ru_query)
                    .then((data) => {
                        if (data && data.forEach) {
                            data.forEach((item, i) => {
                                let value = getValueFromAddress(item);

                                if (!value) {
                                    return;
                                }

                                let isDuplicate = false;
                                for (let j = 0; j < search_result.length; j++) {
                                    if (search_result[j].value == value) {
                                        isDuplicate = true;
                                        break;
                                    }
                                }
                                if (isDuplicate) {
                                    return;
                                }

                                search_result.push({
                                    value: value,
                                    data: item
                                });
                            });
                        }

                        done({ suggestions: search_result });
                    }, () => {
                        search_result.push({
                            value: "Ошибка",
                            data: null
                        });
                        done({ suggestions: search_result });
                    });
            }
        }

        addressSelect(suggestion, selectFn) {
            if (!suggestion.data)
                return;

            selectFn(suggestion.data);
        }

        beforeRender(container, suggestions) {
            let child, city, children = container.children();
            for (let i = 0; i < children.length; i++) {

                let item = suggestions[i].data;
                if (!item || !item.address) {
                    continue;
                }

                child = children[i];
                city = document.createElement("div");
                city.className = "autocomplete-suggestion-city";
                city.innerHTML = item.address.city || item.address.town;
                child.appendChild(city);
            }
        }
    }

    window.createGeoAutocomplete = (input, selectFn) => new GeoAutocomplete(input, selectFn);
}());