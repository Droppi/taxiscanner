﻿; (function () {

    "use strict";

    const ContentInfoWindow = `<button type="button" onclick="map.setByTarget('from')" class="btn btn-primary btn-xs">Отсюда</button>
    <button type="button" onclick="map.setByTarget('to')" class ="btn btn-primary btn-xs">Сюда</button>`;


    class Map {
        constructor() {
            this.map = new google.maps.Map(document.getElementById("map"), {
                zoom: 14,
                center: { lat: 55.01209, lng: 82.93685 },
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: true,
                zoomControl: true
            });

            this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(document.getElementById("routeForm"));

            this.fromMarker = new google.maps.Marker({
                label: "А",
                map: this.map,
                title: "Откуда"
            });

            this.target = new google.maps.InfoWindow({
                content: ContentInfoWindow,
            });

            this.map.addListener("click",
            (args) => {
                let lastMapZoom = this.map.getZoom();
                setTimeout(() => this.showTargetMarker(args, lastMapZoom), 500);
            });

            this.toMarker = new google.maps.Marker({
                label: "Б",
                map: this.map,
                title: "Куда"
            });

            this.startDrag = 0;
            this.map.addListener("dragstart",
            () => {
                window.routeForm.blur();
                if (window.routeForm.isShow()) {
                    this.startDrag++;
                    if (this.startDrag == 3) {
                        window.routeForm.hideRouteForm();
                        this.startDrag = 0;
                    }
                }
            });
        }

        findMe() {
            this.getCurrentLocation(position => {
                let _position = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                this.map.setCenter(_position);
                this.ShowFromMarker(_position);

                window.routeForm.setFrom(_position.lat, _position.lng);
            });
        }

        getCurrentLocation(onSuccess) {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(onSuccess,
                        (error) => this.handleLocationError(true, error),
                        {
                            enableHighAccuracy: true,
                            timeout: 5000,
                            maximumAge: 0
                        });
            } else {
                this.handleLocationError(false);
            }
            return null;
        }

        handleLocationError(browserHasGeolocation, error) {
            console.log(browserHasGeolocation ?
                `Error: The Geolocation service failed. ERROR - ${error.code}, ${error.message}.` :
                "Error: Your browser doesn\'t support geolocation.");
        }

        setByTarget(point) {
            let position = this.target.getPosition();
            switch (point) {
                case "from":
                    this.ShowFromMarker(position);
                    window.routeForm.setFrom(position.lat(), position.lng());
                    break;
                case "to":
                    this.ShowToMarker(position);
                    window.routeForm.setTo(position.lat(), position.lng());
                    break;
            }
        }

        showTargetMarker(args, lastMapZoom) {
            if (lastMapZoom != this.map.getZoom()) {
                return;
            }

            this.target.setPosition(args.latLng);
            this.target.open(this.map);
        }

        ShowFromMarker(position) {
            this.fromMarker.setMap(this.map);
            this.fromMarker.setPosition(position);
            this.target.close();
        }

        HideFromMarker() {
            this.fromMarker.setMap(null);
        }

        ShowToMarker(position) {
            this.toMarker.setMap(this.map);
            this.toMarker.setPosition(position);
            this.target.close();
        }

        HideToMarker() {
            this.toMarker.setMap(null);
        }

        showAllTargets() {
            if (!this.toMarker || !this.fromMarker || !this.toMarker.getPosition() || !this.fromMarker.getPosition()) {
                return;
            }

            let bounds = new google.maps.LatLngBounds();
            bounds.extend(this.toMarker.getPosition());
            bounds.extend(this.fromMarker.getPosition());

            this.map.fitBounds(bounds);
        }
    }

    window.createMap = function () {
        window.map = new Map();
    }
}());