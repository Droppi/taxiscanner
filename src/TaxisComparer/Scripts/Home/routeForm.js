﻿;
(function() {
    "use strict";

    class RouteForm {
        constructor() {
            this.fromInput = $("#fromInput");
            this.toInput = $("#toInput");

            this.fromInput.on("change invalid", function () {
                this.setCustomValidity("");

                if (!this.validity.valid) {
                    this.setCustomValidity("Укажите начальный адрес");
                }
            });

            this.fromInput.on("input", function () {
                window.map.HideFromMarker();
            });

            this.toInput.on("change invalid", function () {
                this.setCustomValidity("");

                if (!this.validity.valid) {
                    this.setCustomValidity("Укажите конечный адрес");
                }
            });

            this.toInput.on("input", function () {
                window.map.HideToMarker();
            });

            this.routeForm = $("#routeForm");
            this.pricePanel = $("#pricePanel");
            this.prices = window.createPrices();
            this.priceBtn = $("#priceBtn");
            this.priceAlerts = $("#priceAlerts");

            this.fromLat = $("#fromLat");
            this.fromLng = $("#fromLng");
            this.toLat = $("#toLat");
            this.toLng = $("#toLng");

            this.fromAddress = { data: null };
            this.toAddress = { data: null };

            this.fromAutocomplete = window.createGeoAutocomplete(this.fromInput,
            (data) => {
                this.fromInput.get(0).setCustomValidity("");
                this.fromAddress.data = data;
                this.fromLat.val(data.lat);
                this.fromLng.val(data.lon);
                window.map.ShowFromMarker({
                    lat: Number(data.lat),
                    lng: Number(data.lon)
                });
            });

            this.toAutocomplete = window.createGeoAutocomplete(this.toInput,
            (data) => {
                this.toInput.get(0).setCustomValidity("");
                this.toAddress.data = data;
                this.toLat.val(data.lat);
                this.toLng.val(data.lon);
                window.map.ShowToMarker({
                    lat: Number(data.lat),
                    lng: Number(data.lon)
                });
            });
        }
        setTo(lat, lon) {
            this.toLat.val(lat);
            this.toLng.val(lon);
            this.setInputValue(this.toAddress, this.toInput, { lat, lon });
            if (!this.isShow()) {
                this.showRouteForm();
            }
        }
        setFrom(lat, lon) {
            this.fromLat.val(lat);
            this.fromLng.val(lon);
            this.setInputValue(this.fromAddress, this.fromInput, { lat, lon });
        }
        setInputValue(address, input, data) {
            return new Promise((resolve, reject) => {
                let _address = address;
                let _input = input;
                window.geocoding.getAddress(data.lat, data.lon)
                    .then((item) => {
                        let value = getValueFromAddress(item);
                        if (!value) {
                            _input.val("Ошибка");
                            reject();
                            return;
                        }

                        _address.data = item;
                        _input.val(value);
                        resolve(item);
                    },
                    (error) => {
                        _input.val("Ошибка");
                    });
            });
        }
        setToAll(lat, lon) {
            return new Promise((resolve, reject) => {
                this.toLat.val(lat);
                this.toLng.val(lon);
                window.map.ShowToMarker({
                    lat: Number(lat),
                    lng: Number(lon)
                });
                this.setInputValue(this.toAddress, this.toInput, { lat, lon }).then(() => resolve());
            });
        }
        setFromAll(lat, lon) {
            return new Promise((resolve, reject) => {
                this.fromLat.val(lat);
                this.fromLng.val(lon);
                window.map.ShowFromMarker({
                    lat: Number(lat),
                    lng: Number(lon)
                });
                this.setInputValue(this.fromAddress, this.fromInput, { lat, lon }).then(() => resolve());
            });
        }
        clearPrices() {
            this.prices.clear();
            this.pricePanel.hide();
            this.priceAlerts.empty();
        }
        showRouteForm() {
            this.routeForm.addClass("route-show");
            this.routeForm.removeClass("route-hide");
            this.routeForm.unbind("click");
        }
        hideRouteForm() {
            this.fromInput.autocomplete("hide");
            this.toInput.autocomplete("hide");
            this.routeForm.removeClass("route-show");
            this.routeForm.addClass("route-hide");
            this.routeForm.click(() => {
                window.routeForm.showRouteForm();
            });
        }
        isShow() {
            return this.routeForm.hasClass("route-show");
        }
        blur() {
            this.fromInput.blur();
            this.toInput.blur();
        }
        addHouseNotFoundMessage() {
            let alertDiv = document.createElement("div");
            alertDiv.id = "houseMessage";
            alertDiv.className = "alert alert-warning";
            alertDiv.innerHTML = "Укажите точные адреса, чтобы узнать больше цен!";

            let closeBtn = document.createElement("a");
            closeBtn.href = "#";
            closeBtn.className = "close";
            closeBtn.setAttribute("data-dismiss", "alert");
            closeBtn.setAttribute("aria-label", "close");
            closeBtn.innerHTML = "&times;";

            alertDiv.appendChild(closeBtn);
            this.priceAlerts.append(alertDiv);
        }

        //Form's events
        formOnSubmit() {
            if (isEmptyOrSpaces(this.fromLat.val()) || isEmptyOrSpaces(this.fromLng.val())) {
                this.fromInput.val("");
                return;
            }

            if (isEmptyOrSpaces(this.toLat.val()) || isEmptyOrSpaces(this.toLng.val())) {
                this.toInput.val("");
                return;
            }

            this.priceBtn.button("loading");
            this.clearPrices();

            if (!this.fromAddress.data.address.house_number || !this.toAddress.data.address.house_number) {
                this.addHouseNotFoundMessage();
            }

            window.map.showAllTargets();

            if (this.fromAddress && this.fromAddress.data) {
                window.storedAddresses.addAddress(this.fromAddress.data);
            }
            if (this.toAddress && this.toAddress.data) {
                window.storedAddresses.addAddress(this.toAddress.data);
            }

            window.historyManager.addNewState({
                startLat: this.fromAddress.data.lat,
                startLon: this.fromAddress.data.lon,
                endLat: this.toAddress.data.lat,
                endLon: this.toAddress.data.lon
            });

            this.pricePanel.show();
        }
        formOnFailure(errorMessage) {
            this.prices.addError(errorMessage);
        }
        formOnComplete() {
            this.priceBtn.button("reset");
            this.showRouteForm();
        }

        //
        addPrice(taxiInfo) {
            this.prices.addPrice(taxiInfo);
        }
        search() {
            window.taxiHub.getPrices(this.fromLat.val(), this.fromLng.val(), this.toLat.val(), this.toLng.val());
            return false;
        }
        searchByState(state) {
            if (!window.historyManager.checkStateValid(state)) {
                return;
            }

            this.setFromAll(state.startLat, state.startLon)
                .then(() => this.setToAll(state.endLat, state.endLon))
                .then(() => document.getElementById("priceBtn").click());
        }
    }

    window.createRouteForm = function () {
        window.routeForm = new RouteForm();
    }
}());


$(document)
    .ready(() => {
        $("#routeForm")
            .on("keyup keypress",
            (e) => {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            });
    });


function getValueFromAddress(data) {
    if (!data || !data.address) {
        return null;
    }

    let value;

    if (data.address.road) {
        value = `${data.address.road}`;
    }
    if (data.address.house_number) {
        value = `${value}, ${data.address.house_number}`;
    }

    return value;
}

function validateAddress(data) {
    if (!data ||
        !data.value ||
        !data.data ||
        !data.data.lat ||
        !data.data.lon ||
        !data.data.address ||
        !data.data.address.house_number ||
        !data.data.address.road ||
        (!data.data.address.city && !data.data.address.town)) {
        return false;
    }
    return true;
}