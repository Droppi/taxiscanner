"use strict";


const serverToken = "-d-JPrT1p-De3Ae7WCHr_uulfoozM--0ZbA_LLk8";
var comparer;
var map;
var markerFrom, markerTo;
var myLocation;
var popup;

window.onload = function init() {
    comparer = new Comparer();
}

DG.then(function () {    
    markerFrom = DG.marker().bindPopup("From");
    markerTo= DG.marker().bindPopup("To");
    popup = DG.popup().setContent(`<input name="fromButton" type="button" value="from" onclick="setFrom()"> 
    <input name="toButton" type="button" value="to" onclick="setTo()">`);
    
    map = DG.map('map', {
        center: [54.98, 82.89],
        zoom: 13,
        fullscreenControl: false,
    });
    
    DG.control.location({position: 'topright'}).addTo(map);

    map.locate({ setView: true, watch: true })
        .on('locationfound', function (e) {
            myLocation = e;
            DG.marker([e.latitude, e.longitude]).addTo(map).bindLabel("You", {static: true});
        })
        .on('locationerror', function (e) {
            console.log(e);
        });

    map.on('click', function (e) {
        popup.setLatLng(e.latlng);
        popup.openOn(map);
    });

});

function getPrices() {
    let from = markerFrom.getLatLng() || { lat: myLocation.latitude, lng: myLocation.longitude};
    let to = markerTo.getLatLng();
    $("div#Prices").text(`from ${from}, to ${to}`);
    
    if(from && to){
        //GetUberPrice(from, to);
        getAtlas(from, to);
    }
}

function setFrom() {
   markerFrom.setLatLng(popup.getLatLng());
   markerFrom.addTo(map)
}

function setTo() {
    markerTo.setLatLng(popup.getLatLng());
    markerTo.addTo(map);
}


class Comparer {
    constructor() {
        this.from;
        this.to;
    }
}

function getAtlas(from, to) {
    DG.ajax({
            url: 'http://catalog.api.2gis.ru/geo/search',
            data: {
                key: 'rudcgu3317',
                version: 1.3,
                q: from.lat + ',' + from.lng
                   },
                    success: function(data) {
                         console.log(data);
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
}


function getUberPrice(from, to) {
   $.ajax({
        type: "GET",
		url: "https://api.uber.com/v1/estimates/price",
        data: {start_latitude: from.lat, start_longitude: from.lng,
               end_latitude: to.lat, end_longitude: to.lng,
                server_token: serverToken},        
        dataType: "json",
        timeout: 10000,
        cache: false,
        success: function (data, textStatus) {
            var price = data.prices[0];
            if(price)    
		      $("div#Prices").append(`<br>${price.localized_display_name} cost ${price.estimate}`);		
		},
		error: function (jqXHR, textStatus, errorThrown) {
			$("div#Prices").append("\nUber is not available.");
		}
    }); 
}