﻿; (function () {
    "use strict";

    class TaxiHub {
        constructor(startCallback) {
            this.taxisHub = $.connection.taxisHub;
            $.connection.hub.start()
                .done(() => startCallback())
                .fail(error => window.logger.error("Hubs connection is failed.", error));

            this.taxisHub.client.addTaxi = taxiInfo => window.routeForm.addPrice(taxiInfo);
            this.taxisHub.client.error = errorMessage => window.routeForm.formOnFailure(errorMessage);
            this.taxisHub.client.endSearch = () => window.routeForm.formOnComplete();
        }

        getPrices(fromLat, fromLng, toLat, toLng) {
            if ($.connection.hub.state !== $.signalR.connectionState.connected) {
                $.connection.hub.start()
                    .done(() => {
                        this.taxisHub.server.getTaxis(fromLat, fromLng, toLat, toLng);
                    });
            } else {
                this.taxisHub.server.getTaxis(fromLat, fromLng, toLat, toLng);
            }
        }
    }

    window.createTaxiHub = function (startCallback) {
        window.taxiHub = new TaxiHub(startCallback);
    }
}());