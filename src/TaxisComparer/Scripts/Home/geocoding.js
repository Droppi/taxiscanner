; (function() {

    "use strict";

    class Nominatim {
        getAddress(lat, lng) {
            return new Promise(function(resolve, reject) {
                $.ajax({
                    type: "GET",
                    url: "https://nominatim.openstreetmap.org/reverse",
                    data: {
                        format: "json",
                        "accept-language": "ru",
                        lat: lat,
                        lon: lng,
                        zoom: 18,
                        addressdetails: 1,
                        email: "postmaster@taxiscanner.pro",
                        extratags: 0,
                        namedetails: 0
                    },
                    dataType: "json",
                    timeout: 10000,
                    success: function(data, textStatus) {
                        if (textStatus === "success") {
                            resolve(data);
                        } else {
                            var error = new Error(textStatus);
                            error.code = this.status;
                            reject(error);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        reject(new Error(textStatus));
                    }
                });
            });
        }

        findAddress(address) {
            return new Promise(function(resolve, reject) {
                $.ajax({
                    type: "GET",
                    url: "https://nominatim.openstreetmap.org/search",
                    data: {
                        format: "json",
                        "accept-language": "ru",
                        q: address,
                        countrycodes: "ru",
                        viewbox: "82.41119,54.68018,83.68423,55.23216",
                        bounded: 1,
                        polygon: 0,
                        addressdetails: 1,
                        email: "postmaster@taxiscanner.pro",
                        limit: 10,
                        extratags: 0,
                        namedetails: 0
                    },
                    dataType: "json",
                    timeout: 10000,
                    success: function (data, textStatus) {
                        if (textStatus === "success") {
                            resolve(data);
                        } else {
                            var error = new Error(textStatus);
                            error.code = this.status;
                            reject(error);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        reject(new Error(textStatus));
                    }
                });
                });
        }
    }

    window.createGeocoding = function () {
        window.geocoding = new Nominatim();
    }
} ());