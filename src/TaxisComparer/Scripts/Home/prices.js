﻿; (function () {
    "use strict";

    const BOOTSTRAP_COLORS = ["#428bca", "#5cb85c", "#5bc0de", "#f0ad4e", "#d9534f"];

    class Prices {
        constructor() {
            this.pricesDiv = $("#prices");
            this.pricesArray = [];
        }

        clear() {
            this.pricesDiv.empty();
            this.pricesArray = [];
        }
        addError(errorMessage) {
            this.pricesDiv.append(`<br/><div class="bg-danger">${errorMessage || "Ошибка!"}</div>`);
        }
        addPrice(taxiInfo) {
            if (!taxiInfo)
                return;

            this.insertTaxiPrice(this.getIndex(taxiInfo), taxiInfo);
        }
        getIndex(taxiInfo) {
            for (var i = 0; i < this.pricesArray.length; i++) {
                if (taxiInfo.Price < this.pricesArray[i].taxiInfo.Price)
                    return i;
            }
            return -1;
        }
        insertTaxiPrice(index, taxiInfo) {
            let taxi = this.createTaxiPrice(taxiInfo);
            let $domElement = $(taxi.domElement);

            if (index === -1) {
                this.pricesArray.push(taxi);
                this.pricesDiv.append($domElement);
            } else {
                this.pricesArray.splice(index, 0, taxi);
                $domElement.insertBefore(this.pricesDiv.children().eq(index));
            }
            $domElement.show("normal");
        }
        createTaxiPrice(taxiInfo) {
            let color = BOOTSTRAP_COLORS[randomInt(BOOTSTRAP_COLORS.length - 1)];

            let rowDiv = document.createElement("div");
            rowDiv.className = "row taxi-price";
            rowDiv.style.display = "none";

            let circleNameParentDin = document.createElement("div");
            circleNameParentDin.className = "col-lg-1 col-md-1 col-sm-1 col-xs-1";
            rowDiv.appendChild(circleNameParentDin);

            let circleNameDin = document.createElement("div");
            circleNameDin.className = "circle-name";
            circleNameDin.style.backgroundColor = `${color}`;
            circleNameDin.innerText = `${taxiInfo.Name.charAt(0)}`;
            circleNameParentDin.appendChild(circleNameDin);

            let taxiNameDiv = document.createElement("div");
            taxiNameDiv.className = "col-lg-4 col-md-5 col-sm-5 col-xs-5 taxi-name";
            rowDiv.appendChild(taxiNameDiv);

            let taxiLing = document.createElement("a");
            taxiLing.target = "_blank";
            taxiLing.href = `${taxiInfo.Site}`;
            taxiLing.innerText = `${taxiInfo.Name}`;
            taxiNameDiv.appendChild(taxiLing);

            let priceDiv = document.createElement("div");
            priceDiv.className = "col-lg-3 col-md-3 col-sm-3 col-xs-4";
            rowDiv.appendChild(priceDiv);

            let priceText = document.createElement("strong");
            priceText.innerHTML = `${taxiInfo.IsExact ? "&nbsp" : "~"}${taxiInfo.Price} руб.`;
            priceDiv.appendChild(priceText);

            return {
                taxiInfo: taxiInfo,
                domElement: rowDiv
            };
        }
    }

    window.createPrices = () => new Prices();
}());