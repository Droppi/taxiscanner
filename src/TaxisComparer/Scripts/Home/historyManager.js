﻿; (function () {
    "use strict";

    class HistoryManager {
        constructor() {
            this.isBlockRequest = false;

            History.Adapter.bind(window, "statechange", () => {
                if (window.historyManager.isBlockRequest) {
                    window.historyManager.isBlockRequest = false;
                    return;
                }
                window.routeForm.searchByState(History.getState().data);
            });
        }

        loadQueryState() {
            this.queryState = {
                startLat: getParameterByName("startLat"),
                startLon: getParameterByName("startLon"),
                endLat: getParameterByName("endLat"),
                endLon: getParameterByName("endLon")
            };
            this.addNewState(this.queryState);
            window.routeForm.searchByState(this.queryState);
        }

        addNewState(state) {
            if (!this.checkStateValid(state) || this.equalseStates(state, History.getState().data)) {
                return;
            }

            this.isBlockRequest = true;
            History.pushState(state, "Такси Новосибирска", `?startLat=${state.startLat}&startLon=${state.startLon}&endLat=${state.endLat}&endLon=${state.endLon}`);
        }

        checkStateValid(state) {
            return !!(state && state.startLat && state.startLon && state.endLat && state.endLon);
        }

        equalseStates(stateA, stateB) {
            return !!(stateA && stateB &&
                stateA.startLat == stateB.startLat &&
                stateA.startLon == stateB.startLon &&
                stateA.endLat == stateB.endLat &&
                stateA.endLon == stateB.endLon);
        }
    }

    window.createHistoryManager = function () {
        window.historyManager = new HistoryManager();
    }
}());