﻿"use strict";

function isEmptyOrSpaces(str) {
    return str === null || str.match(/^ *$/) !== null;
}

$('.has-clear input[type="text"]').on("focus input propertychange", function () {
    var $this = $(this);
    var visible = Boolean($this.val());
    $this.siblings(".form-control-clear").toggleClass("hidden", !visible);
}).on("blur", function () {
    $(this).siblings(".form-control-clear").addClass("hidden");
}).trigger("propertychange");

$(".form-control-clear").on("mousedown", function () {
    return false;
}).click(function () {
    $(this).siblings('input[type="text"]').val("")
      .trigger("propertychange").trigger("input").focus();
});

$(document).ready(() => {
    window.logger = log4javascript.getLogger();

    var ajaxAppender = new log4javascript.AjaxAppender("/api/logger/web");
    ajaxAppender.setThreshold(log4javascript.Level.INFO);
    ajaxAppender.setLayout(new log4javascript.JsonLayout());
    logger.addAppender(ajaxAppender);

    window.onerror = (message, source, lineno, colno, error) => {
        window.logger.error(message, error);
        return false;
    };
});

function auto_layout_keyboard(str) {

    const replacer = {
        "q": "й", "w": "ц", "e": "у", "r": "к", "t": "е", "y": "н", "u": "г",
        "i": "ш", "o": "щ", "p": "з", "[": "х", "]": "ъ", "a": "ф", "s": "ы",
        "d": "в", "f": "а", "g": "п", "h": "р", "j": "о", "k": "л", "l": "д",
        ";": "ж", "'": "э", "z": "я", "x": "ч", "c": "с", "v": "м", "b": "и",
        "n": "т", "m": "ь", ",": "б", ".": "ю", "/": "."
    };

    return str.replace(/[A-z/,.;\'\]\[]/g, (ch) => {
        return ch === ch.toLowerCase() ? replacer[ch] : replacer[ch.toLowerCase()].toUpperCase();
    });

}

function getCookie(name) {
    try {
        var matches = document.cookie.match(new RegExp(
          "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
        
    } catch (e) {
        return undefined;
    } 
}

function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}


function randomInt(length) {
    return Math.round(Math.random() * length);
}

function getParameterByName(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    let regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}