/// <binding BeforeBuild='rebuild' ProjectOpened='watch' />
var gulp = require("gulp"),
    concat = require("gulp-concat"),
    concat_css = require("gulp-concat-css"),
    minifyCss = require("gulp-minify-css"),
    uglify = require("gulp-uglify"),
    autoprefixer = require("gulp-autoprefixer"),
    del = require("del"),
    runSequence = require("run-sequence"),
    rename = require("gulp-rename"),
    babel = require("gulp-babel"),
    sourcemaps = require("gulp-sourcemaps"),
    watch = require("gulp-watch"),
    less = require("gulp-less");

var paths = {
    frontend: {
        css: {
            src: [
              "Style/Home/**/*.css"
            ],
            dest: "wwwroot/css/Home"
        },
        less: {
            src: [
              "Style/Less/Home/*.less"
            ],
            dest: "Style/Home/less",
            bootstrap: "wwwroot/lib/bootstrap/less"
        },
        js: {
            src: [
              "Scripts/Home/*.js",
              "!Scripts/Home/taxis_2gis.js"
            ],
            dest: "wwwroot/js/Home"
        }
    }
}


gulp.task("rebuild", function (cb) {
    runSequence(
      "clean",
      "minify",
      cb
    );
}
);

gulp.task("clean", function () {
    del(paths.frontend.css.dest + "/*");
    del(paths.frontend.js.dest + "/*");
    del(paths.frontend.less.dest + "/*");
});

gulp.task("minify", function (cb) {
    runSequence("frontend:compiling-less", "frontend:minify-css", "frontend:minify-js", cb);
}
);

gulp.task("frontend:compiling-less", function() {
    return gulp.src(paths.frontend.less.src)
        .pipe(less({
            paths: [paths.frontend.less.bootstrap]
        }))
        .pipe(gulp.dest(paths.frontend.less.dest));
});

gulp.task("frontend:minify-css", function () {
    return gulp.src(paths.frontend.css.src)
        .pipe(autoprefixer("last 2 version", "> 5%"))
        .pipe(concat_css("home.css", { rebaseUrls: true }))
        .pipe(gulp.dest(paths.frontend.css.dest))
        .pipe(rename("home.min.css"))
        .pipe(minifyCss())
        .pipe(gulp.dest(paths.frontend.css.dest));
}
);

gulp.task("frontend:minify-js", function () {
    return gulp.src(paths.frontend.js.src)
        .pipe(babel({ presets: ["babel-preset-es2015"] }))
        .pipe(sourcemaps.init())
        .pipe(concat("home.js"))
        .pipe(gulp.dest(paths.frontend.js.dest))
        .pipe(rename("home.min.js"))
        .pipe(uglify())
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest(paths.frontend.js.dest));
}
);

gulp.task("watch", function () {

    gulp.watch(paths.frontend.css.src, { name: "cssWatch" }, function() {
        del(paths.frontend.css.dest + "/*");
        gulp.run("frontend:minify-css");
    });

    gulp.watch(paths.frontend.less.src, { name: "lessWatch" }, function() {
        del(paths.frontend.less.dest + "/*");
        gulp.run("frontend:compiling-less");
    });

    gulp.watch(paths.frontend.js.src, { name: "jsWatch" }, function () {
        del(paths.frontend.js.dest + "/*");
        gulp.run("frontend:minify-js");
    });

});