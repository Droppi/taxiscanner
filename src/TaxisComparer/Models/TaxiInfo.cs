﻿namespace TaxisComparer.Models
{
    public class TaxiInfo
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public bool IsExact { get; set; } = true;

        public string Site { get; set; }

        public string Logo { get; set; }
    }
}
