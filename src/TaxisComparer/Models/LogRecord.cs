﻿using System;
using NLog;
using TaxisComparer.Infrastructure.Common;

namespace TaxisComparer.Models
{
    public enum log4javascript_LogType
    {
        TRACE,
        DEBUG,
        INFO,
        WARN,
        ERROR,
        FATAL,
    }

    public class LogRecord
    {
        public string Data { get; set; }
        public string Layout { get; set; }

        private JsonData[] _jsonData;
        public JsonData[] JsonData => _jsonData ?? (_jsonData = JsonConverter.DeserializeObject<JsonData[]>(Data));
        
    }

    public class JsonData
    {
        public string Logger { get; set; }
        public string Timestamp { get; set; }
        public string Level { get; set; }
        public string Url { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }

        public LogLevel NLogLevel
        {
            get
            {
                switch ((log4javascript_LogType)Enum.Parse(typeof(log4javascript_LogType),Level))
                {
                    case log4javascript_LogType.TRACE:
                        return LogLevel.Trace;
                    case log4javascript_LogType.DEBUG:
                        return LogLevel.Debug;
                    case log4javascript_LogType.INFO:
                        return LogLevel.Info;
                    case log4javascript_LogType.WARN:
                        return LogLevel.Warn;
                    case log4javascript_LogType.ERROR:
                        return LogLevel.Error;
                    case log4javascript_LogType.FATAL:
                        return LogLevel.Fatal;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }
}