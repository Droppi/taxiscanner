﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Net;
using System.Linq;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Extensions.Logging;
using NLog.Web;
using TaxisComparer.Infrastructure.DataBinders;
using TaxisComparer.Infrastructure.Filters;
using TaxisComparer.Infrastructure.IoC;
using TaxisComparer.Infrastructure.Options;
using System.Net.Http;

namespace TaxisComparer
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; set; }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly Dictionary<string, List<Exception>> _exceptions;

        public Startup(IHostingEnvironment env)
        {
            _exceptions = new Dictionary<string, List<Exception>>
            {
                {nameof(Startup), new List<Exception>()},
                {nameof(ConfigureServices), new List<Exception>()},
            };

            try
            {
                var builder = new ConfigurationBuilder()
                    .SetBasePath(env.ContentRootPath)
                    .AddJsonFile("appsettings.json", true, true)
                    .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
                    .AddJsonFile("taxi.json")
                    .AddEnvironmentVariables();

                Configuration = builder.Build();
            }
            catch (Exception exception)
            {
                _exceptions[nameof(Startup)].Add(exception);
            }
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            try
            {
                services.Configure<GzipCompressionProviderOptions>
                    (options => options.Level = CompressionLevel.Fastest);
                services.AddResponseCompression(options =>
                {
                    options.EnableForHttps = true;
                    options.Providers.Add<GzipCompressionProvider>();
                });

                services.AddSignalR(options => { options.Hubs.EnableDetailedErrors = true; });

                var builder = services.AddMvc(
                    options => options.ModelBinderProviders.Insert(0, new DoubleModelBinderProvider()));
                builder.AddMvcOptions(options => { options.Filters.Add(new GlobalExceptionFilter()); });

                services.AddTaxiOptions(Configuration);

                var containerBuilder = new ContainerBuilder();
                containerBuilder.RegisterModule<ServicesRegister>();
                containerBuilder.RegisterModule<TaxisRegister>();
                containerBuilder.Populate(services);

                containerBuilder.RegisterInstance(Configuration);
                containerBuilder.Register(c => new HttpClient(new HttpClientHandler() {UseCookies = false}))
                    .As<HttpClient>()
                    .SingleInstance();

                var container = containerBuilder.Build();
                return container.Resolve<IServiceProvider>();
            }
            catch (Exception exception)
            {
                _exceptions[nameof(ConfigureServices)].Add(exception);
                return null;
            }
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            env.ConfigureNLog("nlog.config");
            loggerFactory.AddNLog();
            app.AddNLogWeb();

            if (_exceptions.Any(p => p.Value.Any()))
            {
                app.Run(
                    async context =>
                    {
                        context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                        context.Response.ContentType = "text/plain";

                        foreach (var exception in _exceptions)
                        {
                            foreach (var val in exception.Value)
                            {
                                Logger.Error(val, $"{nameof(Startup)}|{exception.Key}");
                                await context.Response
                                    .WriteAsync($"Error on {exception.Key}: {val.Message}|{val.StackTrace}")
                                    .ConfigureAwait(false);
                            }
                        }
                    });
                return;
            }

            try
            {
                if (env.IsDevelopment())
                {
                    app.UseBrowserLink();
                    app.UseDeveloperExceptionPage();
                }
                else
                {
                    app.UseExceptionHandler("/Shared/Error");
                }

#if !DEBUG
                app.UseRewriter(new RewriteOptions()
                    .AddRedirectToHttpsPermanent());
#endif

                app.UseResponseCompression();

                app.UseStaticFiles(new StaticFileOptions
                {
                    OnPrepareResponse = context =>
                    {
                        if (!string.IsNullOrEmpty(context.Context.Request.Query["v"]))
                        {
                            context.Context.Response.Headers.Add("Cache-Control", new[] {"public,max-age=31536000"});
                            context.Context.Response.Headers.Add("Expires",
                                new[] {DateTime.UtcNow.AddYears(1).ToString("R")});
                        }
                    }
                });

                app.UseMvc(routes =>
                {
                    routes.MapRoute(
                        name: "default",
                        template: "{controller=Home}/{action=Index}/{id?}");
                });

                app.UseWebSockets();
                app.UseSignalR();

                app.Run(async (context) =>
                {
                    var logger = loggerFactory.CreateLogger("Catchall Endpoint");
                    logger.LogWarning("No endpoint found for request {path}", context.Request.Path);
                    context.Response.StatusCode = (int) HttpStatusCode.NotFound;
                    await context.Response.WriteAsync("No endpoint found.");
                });
            }
            catch (Exception exception)
            {
                app.Run(
                    async context =>
                    {
                        Logger.Error(exception, $"{nameof(Startup)}|{nameof(Configure)}");

                        context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                        context.Response.ContentType = "text/plain";

                        await context.Response.WriteAsync($"{exception.Message}|{exception.StackTrace}")
                            .ConfigureAwait(false);
                    });
            }
        }
    }
}