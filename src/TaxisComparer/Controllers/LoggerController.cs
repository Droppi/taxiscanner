﻿using System;
using Microsoft.AspNetCore.Mvc;
using NLog;
using TaxisComparer.Infrastructure;
using TaxisComparer.Models;

namespace TaxisComparer.Controllers
{
    [Route("api/logger")]
    public class LoggerController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static readonly Logger WebLogger = LogManager.GetLogger("WebLogger");

        [HttpPost("web")]
        public void Post(LogRecord logRecord)
        {
            try
            {
                if(logRecord.JsonData == null)
                    throw new ArgumentNullException(nameof(logRecord.JsonData));

                foreach (var data in logRecord.JsonData)
                {
                    WebLogger.Log(data.NLogLevel, $"{data.Timestamp}| {data.Message}| {data.Exception}");
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception, $@"{nameof(LoggerController)}|Web| {logRecord.Layout}| {logRecord.Data}");
            }
        }
    }
}
