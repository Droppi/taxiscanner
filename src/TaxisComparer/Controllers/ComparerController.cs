﻿using System;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using NLog;
using TaxisComparer.Infrastructure;
using TaxisComparer.Infrastructure.Comparer;

namespace TaxisComparer.Controllers
{
    [Route("api/[controller]")]
    public class ComparerController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private IPriceComparer PriceComparer { get; }

        public ComparerController([FromServices]IPriceComparer priceComparer)
        {
            PriceComparer = priceComparer;
        }

        [HttpGet("prices")]
        public IActionResult PricesAsync()
        {
            return RedirectToActionPermanent("Index", "Home");
        }

        [HttpPost("prices/geo")]
        public IActionResult PricesGeoAsync(double fromLat, double fromLng, double toLat, double toLng)
        {
            if (fromLat <= 0 || fromLng <= 0)
            {
                return BadRequest("Некорректный начальный адрес!");
            }

            if (toLat <= 0 || toLng <= 0)
            {
                return BadRequest("Некорректный конечный адрес!");
            }

            try
            {
                var result = PriceComparer.GetPriceComparisonAsync(new Point(fromLat, fromLng), new Point(toLat, toLng));

                if (!result.IsSuccess)
                {
                    return BadRequest(result.Message);
                }
                if (result.Value == null || !result.Value.Any())
                {
                    return NoContent();
                }

                return Json(result.Value);
            }
            catch (Exception exception)
            {
                Logger.Error(exception, $@"{nameof(ComparerController)}|{nameof(PricesAsync)}| {nameof(fromLat)}={fromLat},{nameof(fromLng)}={fromLng},{nameof(toLat)}={toLat},{nameof(toLng)}={toLng}");
                return StatusCode((int) HttpStatusCode.InternalServerError, "Ошибка на серевере!");
            }
        }

        [HttpPost("prices/str")]
        public IActionResult PricesByStrAsync(string from, string to)
        {
            if (string.IsNullOrWhiteSpace(from))
            {
                return BadRequest("Нет начального адреса!");
            }
            if (string.IsNullOrWhiteSpace(to))
            {
                return BadRequest("Нет конечного адреса!");
            }

            try
            {
                var result = PriceComparer.GetPriceComparisonAsync(from, to);
                if (!result.IsSuccess)
                {
                    return BadRequest(result.Message);
                }
                if (result.Value == null || !result.Value.Any())
                {
                    return NoContent();
                }

                return Json(result.Value);
            }
            catch (Exception exception)
            {
                Logger.Error(exception, $@"{nameof(ComparerController)}|{nameof(PricesAsync)}| {nameof(from)}={from},{nameof(to)}={to}");
                return StatusCode((int)HttpStatusCode.InternalServerError, "Ошибка на серевере!");
            }
        }
    }
}
