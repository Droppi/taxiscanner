﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using NLog;
using TaxisComparer.Infrastructure;
using TaxisComparer.Infrastructure.Comparer;

namespace TaxisComparer.Hubs
{
    public class TaxisHub : Hub
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private IPriceComparer PriceComparer { get; }

        public TaxisHub([FromServices]IPriceComparer priceComparer)
        {
            PriceComparer = priceComparer;
        }

        public async void GetTaxis(double fromLat, double fromLng, double toLat, double toLng)
        {
            try
            {
                if (fromLat <= 0 || fromLng <= 0)
                {
                    Clients.Caller.Error("Некорректный начальный адрес!");
                    return;
                }

                if (toLat <= 0 || toLng <= 0)
                {
                    Clients.Caller.Error("Некорректный конечный адрес!");
                    return;
                }

                await PriceComparer.GetPriceComparisonAsync(new Point(fromLat, fromLng), new Point(toLat, toLng),
                    (taxiInfo) =>
                    {
                        Clients.Caller.AddTaxi(taxiInfo);
                    });

            }
            catch (InvalidOperationException exception)
            {
                Clients.Caller.Error(exception.Message);
            }
            catch (Exception exception)
            {
                Clients.Caller.Error("Ошибка на серевере!");
                Logger.Error(exception,$@"{nameof(TaxisHub)}|{nameof(GetTaxis)}| {nameof(fromLat)}={fromLat},{nameof(fromLng)}={fromLng},{nameof(toLat)}={toLat},{nameof(toLng)}={toLng}");
            }
            finally
            {
                Clients.Caller.EndSearch();
            }
        }
    }
}
